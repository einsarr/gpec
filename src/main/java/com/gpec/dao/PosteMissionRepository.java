package com.gpec.dao;

import com.gpec.model.PosteMission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PosteMissionRepository extends JpaRepository<PosteMission, Integer> {
}
