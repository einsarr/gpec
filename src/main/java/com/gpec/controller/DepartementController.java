package com.gpec.controller;

import com.gpec.dao.DepartementRepository;
import com.gpec.model.Departement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class DepartementController implements Serializable {
    @Autowired
    private DepartementRepository departementRepository;

    @GetMapping("/departements")
    public String departementPage(Model model) {
        Departement departement = new Departement();
        model.addAttribute("departement", departement);
        List<Departement> departements = departementRepository.findAll();
        model.addAttribute("departements", departements);
        return "employes/departements";
    }
    @PostMapping("/addDepartement")
    public String addcorps(@ModelAttribute("departement") Departement departement) {
        try {
            departementRepository.save(departement);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/departements";
    }

    @GetMapping("/departement/{id}")
    public @ResponseBody
    Departement getDepartement(@PathVariable(name = "id") int departementId){
        return departementRepository.findById(departementId).get();
    }


    @PostMapping("/removeDepartement")
    public String removeDepartement(int departementId) {
        Departement g = departementRepository.findById(departementId).get();
        departementRepository.delete(g);
        return "redirect:/departements";
    }
}
