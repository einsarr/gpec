package com.gpec.controller;

import com.gpec.dao.ActiviteRepository;
import com.gpec.model.Activite;
import com.gpec.service.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class ActiviteController implements Serializable {
    @Autowired
    private ActiviteRepository activiteRepository;
    @Autowired
    private Utils utils;
    @GetMapping("/activites")
    public String ActivitesPage(Model model) {
        Activite activite = new Activite();
        model.addAttribute("activite", activite);
        List<Activite> activites = activiteRepository.findAll();
        model.addAttribute("activites", activites);
        return "parametrage/activites";
    }
    @PostMapping("/addActivite")
    public String addActivite(@ModelAttribute("activite") Activite activite){
        try {
            if(activite.getId()==null){
                long maxId   =   activiteRepository.getLastActivite()+1;
                activite.setCode_activite("P"+utils.codification((int)maxId,4));
            }
            activiteRepository.save(activite);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/activites";
    }

    @GetMapping("/activite/{id}")
    public @ResponseBody
    Activite getActivite(@PathVariable(name = "id") int activiteId){
        return activiteRepository.findById(activiteId).get();
    }

    @PostMapping("/removeActivite")
    public String removeActivite(int activiteId) {
        Activite g = activiteRepository.findById(activiteId).get();
        activiteRepository.delete(g);
        return "redirect:/activites";
    }
}
