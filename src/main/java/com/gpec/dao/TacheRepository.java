package com.gpec.dao;

import com.gpec.model.Competence;
import com.gpec.model.Tache;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TacheRepository extends JpaRepository<Tache, Integer> {

    @Query(value = "SELECT * FROM tache t join poste_tache pt " +
            "on pt.tache_id=t.id join poste p on pt.poste_id=p.id WHERE p.id=:posteId",nativeQuery = true)
    public List<Tache> getTacheByPoste(@Param("posteId")Long posteId);



}
