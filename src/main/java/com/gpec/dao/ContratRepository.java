package com.gpec.dao;

import com.gpec.model.Contrat;
import com.gpec.model.Employe;
import com.gpec.model.Poste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratRepository extends JpaRepository<Contrat, Integer> {

    /**
     * Nombre d'employé en fin de contrat
     * @return
     */
    @Query(value = "SELECT COUNT(e.id) FROM employe e JOIN contrat c " +
            "ON c.employe_id=e.id JOIN type_contrat tc ON c.type_contrat_id=tc.id WHERE datediff(c.date_fin,c.date_debut)<=60",nativeQuery = true)
    int countEmpFinContrat();

    @Query(value = "SELECT * FROM employe e JOIN contrat c " +
            "ON c.employe_id=e.id JOIN type_contrat tc ON c.type_contrat_id=tc.id WHERE datediff(c.date_fin,c.date_debut)<=60",nativeQuery = true)
    List<Employe> listeEmpFinContrat();

}
