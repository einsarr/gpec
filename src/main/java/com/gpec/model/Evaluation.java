package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
//@Table(name="evaluation")
@Table(uniqueConstraints = { @UniqueConstraint(name = "UniqueNumberAndStatus", columnNames = { "employe_id", "annee_id" }) })
public class Evaluation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "employe_id")
    private Employe employe;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "annee_id")
    private Annee annee;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "niveau_maitrise_id")
    private NiveauMaitriseCptRequis niveauMaitriseCptAquis;

    //    @Enumerated(EnumType.ORDINAL)
//    private NiveauMaitriseRCpt niveau_maitrise_cpt;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "competence_id")
    private Competence competence;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Competence getCompetence() {
        return competence;
    }

    public void setCompetence(Competence competence) {
        this.competence = competence;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public Annee getAnnee() {
        return annee;
    }

    public void setAnnee(Annee annee) {
        this.annee = annee;
    }

    public NiveauMaitriseCptRequis getNiveauMaitriseCptAquis() {
        return niveauMaitriseCptAquis;
    }

    public void setNiveauMaitriseCptAquis(NiveauMaitriseCptRequis niveauMaitriseCptAquis) {
        this.niveauMaitriseCptAquis = niveauMaitriseCptAquis;
    }
}
