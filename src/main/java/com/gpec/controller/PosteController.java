package com.gpec.controller;

import com.gpec.dao.EmploiTypeRepository;
import com.gpec.dao.ExigencePosteRepository;
import com.gpec.dao.PosteRepository;
import com.gpec.model.EmploiType;
import com.gpec.model.ExigencePoste;
import com.gpec.model.Poste;
import com.gpec.service.FichePoste;
import com.gpec.service.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@PreAuthorize("hasAuthority('ROLE_DIRECTEUR')")
public class PosteController implements Serializable {
    @Autowired
    private PosteRepository posteRepository;

    @Autowired
    private ExigencePosteRepository exigencePosteRepository;

    @Autowired
    private FichePoste fichePoste;

    @Autowired
    private Utils utils;

    @Autowired
    private EmploiTypeRepository emploiTypeRepository;

    @GetMapping("/postes")
    public String postePage(Model model) {
        Poste poste = new Poste();
        model.addAttribute("poste", poste);
        List<ExigencePoste> exigence_postes = exigencePosteRepository.findAll();
        List<EmploiType> emploiTypes = emploiTypeRepository.findAll();
        List<Poste> postes = posteRepository.findAll();
        model.addAttribute("exigence_postes", exigence_postes);
        model.addAttribute("emploiTypes", emploiTypes);
        model.addAttribute("postes", postes);
        return "parametrage/postes";
    }
    @PostMapping("/addPoste")
    public String addPoste(@ModelAttribute("poste")Poste poste) {
        if(poste.getId()==null){
            long maxId   =   posteRepository.getLastPoste()+1;
            poste.setCode_poste("P"+utils.codification((int)maxId,4));
        }
        try {
            posteRepository.save(poste);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/postes";
    }

    @GetMapping("/poste/{id}")
    public @ResponseBody
    Poste getPoste(@PathVariable(name = "id") long posteId){
        return posteRepository.findById(posteId).get();
    }


    @PostMapping("/removePoste")
    public String removePoste(long posteId) {
        Poste g = posteRepository.findById(posteId).get();
        posteRepository.delete(g);
        return "redirect:/postes";
    }

    @GetMapping(value = "/generateFichePoste", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> ficheDePoste(@RequestParam(name = "id") Long id) throws IOException {

        Poste poste = posteRepository.findPosteById(id);

        ByteArrayInputStream bis = FichePoste.employeesReport(poste);

        HttpHeaders headers = new HttpHeaders();

        headers.add("Content-Disposition", "attachment;filename=fichedeposte_"+poste.getCode_poste()+".pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    @GetMapping(value = "/pdfgenerate", produces = MediaType.APPLICATION_PDF_VALUE)
    public void generatePDF(HttpServletResponse response,@RequestParam(name = "id") Long id) throws IOException {
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd:hh:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=pdf_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        this.fichePoste.export(response,posteRepository.findById(id).get());
    }
}
