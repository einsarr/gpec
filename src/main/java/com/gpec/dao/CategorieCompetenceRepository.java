package com.gpec.dao;

import com.gpec.model.CategorieCompetence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategorieCompetenceRepository extends JpaRepository<CategorieCompetence, Integer> {
}
