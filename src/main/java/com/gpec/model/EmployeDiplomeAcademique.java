package com.gpec.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "UniqueNumberAndStatus", columnNames = { "employe_id", "diplome_acad_id" }) })
public class EmployeDiplomeAcademique implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "employe_id")
    private Employe employe;
    @ManyToOne
    @JoinColumn(name = "diplome_acad_id")
    private DiplomeAcademique diplome_academique;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date_dipl_acad;

    @ManyToOne
    @JoinColumn(name = "ecole_id")
    private UniversiteEcoleInstitut universite_ecole_institut;

    public UniversiteEcoleInstitut getUniversite_ecole_institut() {
        return universite_ecole_institut;
    }

    public void setUniversite_ecole_institut(UniversiteEcoleInstitut universite_ecole_institut) {
        this.universite_ecole_institut = universite_ecole_institut;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public DiplomeAcademique getDiplome_academique() {
        return diplome_academique;
    }

    public void setDiplome_academique(DiplomeAcademique diplome_academique) {
        this.diplome_academique = diplome_academique;
    }

    public Date getDate_dipl_acad() {
        return date_dipl_acad;
    }

    public void setDate_dipl_acad(Date date_dipl_acad) {
        this.date_dipl_acad = date_dipl_acad;
    }
}
