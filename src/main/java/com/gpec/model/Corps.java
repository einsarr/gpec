package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Corps implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String libelle_corps;
    private int etat_corps;

    @JsonIgnore
    @OneToMany(mappedBy = "corps")
    private List<Employe> employes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle_corps() {
        return libelle_corps;
    }

    public void setLibelle_corps(String libelle_corps) {
        this.libelle_corps = libelle_corps;
    }

    public int getEtat_corps() {
        return etat_corps;
    }

    public void setEtat_corps(int etat_corps) {
        this.etat_corps = etat_corps;
    }

    public List<Employe> getEmployes() {
        return employes;
    }

    public void setEmployes(List<Employe> employes) {
        this.employes = employes;
    }
}
