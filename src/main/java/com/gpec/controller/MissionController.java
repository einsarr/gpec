package com.gpec.controller;

import com.gpec.dao.MissionRepository;
import com.gpec.model.Mission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Controller
@PreAuthorize("hasAuthority('ROLE_DIRECTEUR')")
public class MissionController implements Serializable {
    @Autowired
    private MissionRepository missionRepository;

    @GetMapping("/missions")
    public String missionPage(Model model) {
        Mission mission = new Mission();
        model.addAttribute("mission", mission);
        List<Mission> missions = missionRepository.findAll();
        model.addAttribute("missions", missions);
        return "parametrage/missions";
    }
    @PostMapping("/addMission")
    public String addMission(@ModelAttribute("mission") Mission mission) {
        try {
            missionRepository.save(mission);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/missions";
    }

    @GetMapping("/mission/{id}")
    public @ResponseBody
    Mission getMission(@PathVariable(name = "id") int missionId){
        return missionRepository.findById(missionId).get();
    }


    @PostMapping("/removeMission")
    public String removeMission(int missionId) {
        Mission g = missionRepository.findById(missionId).get();
        missionRepository.delete(g);
        return "redirect:/missions";
    }
}
