package com.gpec.controller;

import com.gpec.dao.GradeRepository;
import com.gpec.model.Grade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class GradeController implements Serializable {
    @Autowired
    private GradeRepository gradeRepository;

    @GetMapping("/grades")
    public String corpsPage(Model model) {
        Grade grade = new Grade();
        model.addAttribute("grade", grade);
        List<Grade> grades = gradeRepository.findAll();
        model.addAttribute("grades", grades);
        return "parametrage/grades";
    }
    @PostMapping("/addGrade")
    public String addcorps(@ModelAttribute("grade") Grade grade) {
        try {
            gradeRepository.save(grade);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/grades";
    }

    @GetMapping("/grade/{id}")
    public @ResponseBody
    Grade getGrade(@PathVariable(name = "id") int gradeId){
        return gradeRepository.findById(gradeId).get();
    }


    @PostMapping("/removeGrade")
    public String removeGrade(int gradeId) {
        Grade g = gradeRepository.findById(gradeId).get();
        gradeRepository.delete(g);
        return "redirect:/grades";
    }
}
