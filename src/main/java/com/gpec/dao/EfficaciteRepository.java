package com.gpec.dao;

import com.gpec.model.Efficacite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EfficaciteRepository extends JpaRepository<Efficacite, Integer> {
}
