package com.gpec.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class EffectifAnnee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int effectif_janvier;
    private int effectif_h_janvier;
    private int effectif_f_janvier;
    private int effectif_entrant;
    private int effectif_sortant;
    private int etat;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "annee_id")
    private Annee annee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEffectif_janvier() {
        return effectif_janvier;
    }

    public void setEffectif_janvier(int effectif_janvier) {
        this.effectif_janvier = effectif_janvier;
    }

    public int getEffectif_entrant() {
        return effectif_entrant;
    }

    public void setEffectif_entrant(int effectif_entrant) {
        this.effectif_entrant = effectif_entrant;
    }

    public int getEffectif_sortant() {
        return effectif_sortant;
    }

    public void setEffectif_sortant(int effectif_sortant) {
        this.effectif_sortant = effectif_sortant;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public Annee getAnnee() {
        return annee;
    }

    public void setAnnee(Annee annee) {
        this.annee = annee;
    }

    public int getEffectif_h_janvier() {
        return effectif_h_janvier;
    }

    public void setEffectif_h_janvier(int effectif_h_janvier) {
        this.effectif_h_janvier = effectif_h_janvier;
    }

    public int getEffectif_f_janvier() {
        return effectif_f_janvier;
    }

    public void setEffectif_f_janvier(int effectif_f_janvier) {
        this.effectif_f_janvier = effectif_f_janvier;
    }
}
