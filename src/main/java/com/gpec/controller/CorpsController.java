package com.gpec.controller;
import com.gpec.dao.CorpsRepository;
import com.gpec.model.Corps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Controller
public class CorpsController implements Serializable {
    @Autowired
    private CorpsRepository corpsRepository;

    @GetMapping("/corps")
    public String corpsPage(Model model) {
        Corps corp = new Corps();
        model.addAttribute("corps", corp);
        List<Corps> corps_s = corpsRepository.findAll();
        model.addAttribute("corp_s", corps_s);
        return "parametrage/corps";
    }
    @PostMapping("/addCorps")
    public String addcorps(@ModelAttribute("corps") Corps corps) {
        try {
            corpsRepository.save(corps);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/corps";
    }

    @GetMapping("/corps/{id}")
    public @ResponseBody
    Corps getCorps(@PathVariable(name = "id") int corpsId){
        return corpsRepository.findById(corpsId).get();
    }

    /*@GetMapping(value="/removeCorps/{id}")
    public String removeCorps(@PathVariable("id") int id) {
        Corps c = corpsRepository.findById(id).get();
        corpsRepository.delete(c);
        return "redirect:/corps";
    }*/

    @PostMapping("/removeCorps")
    public String removeCorps(int corpsId) {
        Corps c = corpsRepository.findById(corpsId).get();
        corpsRepository.delete(c);
        return "redirect:/corps";
    }
}
