package com.gpec.dao;

import com.gpec.model.DiplomeProfessionel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiplomeProfessionnelRepository extends JpaRepository<DiplomeProfessionel, Integer> {
}
