package com.gpec.dao;

import com.gpec.model.Potentiel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PotentielRepository extends JpaRepository<Potentiel, Integer> {
}
