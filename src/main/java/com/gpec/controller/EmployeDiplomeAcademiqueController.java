package com.gpec.controller;

import com.gpec.dao.*;
import com.gpec.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class EmployeDiplomeAcademiqueController implements Serializable {
    @Autowired
    private EmployeDiplomeAcadRepository employeDiplomeAcadRepository;
    @Autowired
    private EmployeRepository employeRepository;
    @Autowired
    private DiplomeAcademiqueRepository diplomeAcademiqueRepository;
    @Autowired
    private UniversiteEcoleInstitutRepository universiteEcoleInstitutRepository;

    @GetMapping("/employe_dipl_acads")
    public String employediplomeacademiquePage(Model model) {
        EmployeDiplomeAcademique employe_diplome_academique         = new EmployeDiplomeAcademique();
        List<UniversiteEcoleInstitut> universiteEcoleInstituts      = universiteEcoleInstitutRepository.findAll();
        List<Employe> employes                                      = employeRepository.findAll();
        List<DiplomeAcademique> diplomeAcademiques                  = diplomeAcademiqueRepository.findAll();
        List<EmployeDiplomeAcademique> employe_diplome_academiques  = employeDiplomeAcadRepository.findAll();
        model.addAttribute("universite_ecole_instituts", universiteEcoleInstituts);
        model.addAttribute("employes", employes);
        model.addAttribute("diplome_academiques", diplomeAcademiques);
        model.addAttribute("employe_diplome_academique", employe_diplome_academique);
        model.addAttribute("employe_diplome_academiques", employe_diplome_academiques);
        return "employes/employe_diplome_academiques";
    }
    @PostMapping("/addEmployeDiplomeAcademique")
    public String adddiplacad(@ModelAttribute("employe_diplome_academique") EmployeDiplomeAcademique employe_diplome_academique) {
        try {
            employeDiplomeAcadRepository.save(employe_diplome_academique);
        }catch (Exception e){
           e.printStackTrace();
        }
        return "redirect:/employe_dipl_acads";
    }

    @GetMapping("/employe_diplome_academique/{id}")
    public @ResponseBody
    EmployeDiplomeAcademique getEmployeDiplomeAcademique(@PathVariable(name = "id") int employe_diplome_academiqueId){
        return employeDiplomeAcadRepository.findById(employe_diplome_academiqueId).get();
    }


    @PostMapping("/removeEmployeDiplomeAcademique")
    public String removeEmployeDiplomeAcademique(int employe_diplome_academiqueId) {
        EmployeDiplomeAcademique g = employeDiplomeAcadRepository.findById(employe_diplome_academiqueId).get();
        employeDiplomeAcadRepository.delete(g);
        return "redirect:/employe_dipl_acads";
    }
}
