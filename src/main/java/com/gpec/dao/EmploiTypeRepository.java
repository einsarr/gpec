package com.gpec.dao;

import com.gpec.model.EmploiType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmploiTypeRepository extends JpaRepository<EmploiType, Integer> {
}
