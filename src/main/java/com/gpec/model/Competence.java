package com.gpec.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
@Entity
public class Competence implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique=true)
    private String code_competence;
    @Column(unique=true)
    private String libelle_competence;
    private int etat_competence;
    private int est_matrice;

    @ManyToOne
    @JoinColumn(name = "niveau_maitrise_id")
    private NiveauMaitriseCptRequis niveauMaitriseCptRequis;

    @ManyToOne
    @JoinColumn(name = "categorie_id")
    private CategorieCompetence categorie;

    @JsonIgnore
    @OneToMany(mappedBy = "competence")
    private List<PosteCompetence> poste_competences;

    @JsonIgnore
    @OneToMany(mappedBy = "competence")
    private List<Evaluation> evaluations;

//    @Enumerated(EnumType.ORDINAL)
//    private NiveauMaitriseRCpt niveauMaitriseRCpt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle_competence() {
        return libelle_competence;
    }

    public void setLibelle_competence(String libelle_competence) {
        this.libelle_competence = libelle_competence;
    }

    public int getEtat_competence() {
        return etat_competence;
    }

    public void setEtat_competence(int etat_competence) {
        this.etat_competence = etat_competence;
    }

    public List<PosteCompetence> getPoste_competences() {
        return poste_competences;
    }

    public void setPoste_competences(List<PosteCompetence> poste_competences) {
        this.poste_competences = poste_competences;
    }

    public NiveauMaitriseCptRequis getNiveauMaitriseCptRequis() {
        return niveauMaitriseCptRequis;
    }

    public void setNiveauMaitriseCptRequis(NiveauMaitriseCptRequis niveauMaitriseCptRequis) {
        this.niveauMaitriseCptRequis = niveauMaitriseCptRequis;
    }

    public CategorieCompetence getCategorie() {
        return categorie;
    }

    public void setCategorie(CategorieCompetence categorie) {
        this.categorie = categorie;
    }

    public String getCode_competence() {
        return code_competence;
    }

    public void setCode_competence(String code_competence) {
        this.code_competence = code_competence;
    }

    public List<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }

    public int getEst_matrice() {
        return est_matrice;
    }

    public void setEst_matrice(int est_matrice) {
        this.est_matrice = est_matrice;
    }
}
