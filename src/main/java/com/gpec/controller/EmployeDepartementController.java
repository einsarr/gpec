package com.gpec.controller;

import com.gpec.dao.EmployeDepartementRepository;
import com.gpec.dao.EmployeRepository;
import com.gpec.dao.DepartementRepository;
import com.gpec.model.Employe;
import com.gpec.model.EmployeDepartement;
import com.gpec.model.Departement;
import com.gpec.model.EmployePoste;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Controller
public class EmployeDepartementController implements Serializable {
    @Autowired
    private EmployeDepartementRepository employeDepartementRepository;
    @Autowired
    private EmployeRepository employeRepository;
    @Autowired
    private DepartementRepository departementRepository;

    @GetMapping("/employe_departements")
    public String EmployeDepartementsPage(Model model) {
        EmployeDepartement employe_departement          = new EmployeDepartement();
        List<EmployeDepartement> employe_departements   = employeDepartementRepository.findAll();
        List<Employe> employes                          = employeRepository.findAll();
        List<Departement> departements                  = departementRepository.findAll();
        model.addAttribute("employe_departement", employe_departement);
        model.addAttribute("employe_departements", employe_departements);
        model.addAttribute("employes", employes);
        model.addAttribute("departements", departements);
        return "employes/employe_departements";
    }
    @Transactional
    @PostMapping("/addEmployeDepartement")
    public String addEmployeDepartement(@ModelAttribute("employe_departement") EmployeDepartement employe_departement) {
        if(employe_departement.getEmploye()!=null){
            for(EmployeDepartement empd: employe_departement.getEmploye().getEmploye_departements()){
                if(empd.getEtat()==1){
                    empd.setEtat(0);
                    empd.setDate_fin(new Date());
                    try {
                        employeDepartementRepository.save(empd);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
        try {
            employeDepartementRepository.save(employe_departement);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/employe_departements";
    }

    @GetMapping("/employe_departement/{id}")
    public @ResponseBody
    EmployeDepartement getEmployeDepartement(@PathVariable(name = "id") Long employe_departementId){
        return employeDepartementRepository.findById(employe_departementId).get();
    }


    @PostMapping("/removeEmployeDepartement")
    public String removeEmployeDepartement(Long employe_departementId) {
        EmployeDepartement g = employeDepartementRepository.findById(employe_departementId).get();
        employeDepartementRepository.delete(g);
        return "redirect:/employe_departements";
    }
}
