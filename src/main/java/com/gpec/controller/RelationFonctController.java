package com.gpec.controller;

import com.gpec.dao.RelationFonctionnelleRepository;
import com.gpec.model.RelationFonctionnelle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class RelationFonctController implements Serializable {
    @Autowired
    private RelationFonctionnelleRepository relationFonctionnelleRepository;

    @GetMapping("/relation_fonctionnelles")
    public String relationPage(Model model) {
        RelationFonctionnelle relation_fonctionnelle = new RelationFonctionnelle();
        model.addAttribute("relation_fonctionnelle", relation_fonctionnelle);
        List<RelationFonctionnelle> relation_fonctionnelles = relationFonctionnelleRepository.findAll();
        model.addAttribute("relation_fonctionnelles", relation_fonctionnelles);
        return "parametrage/relation_fonctionnelles";
    }
    @PostMapping("/addRelationFonctionnelle")
    public String addRelation(@ModelAttribute("relation_fonctionnelle") RelationFonctionnelle relation_fonctionnelle) {
        try {
            relationFonctionnelleRepository.save(relation_fonctionnelle);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/relation_fonctionnelles";
    }

    @GetMapping("/relation_fonctionnelle/{id}")
    public @ResponseBody
    RelationFonctionnelle getRelationFonctionnelle(@PathVariable(name = "id") int relation_fonctionnelleId){
        return relationFonctionnelleRepository.findById(relation_fonctionnelleId).get();
    }


    @PostMapping("/removeRelationFonctionnelle")
    public String removeRelationFonctionnelle(int relation_fonctionnelleId) {
        RelationFonctionnelle g = relationFonctionnelleRepository.findById(relation_fonctionnelleId).get();
        relationFonctionnelleRepository.delete(g);
        return "redirect:/relation_fonctionnelles";
    }
}
