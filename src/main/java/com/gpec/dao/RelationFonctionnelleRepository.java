package com.gpec.dao;

import com.gpec.model.RelationFonctionnelle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RelationFonctionnelleRepository extends JpaRepository<RelationFonctionnelle, Integer> {
}
