package com.gpec.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "UniqueNumberAndStatus", columnNames = { "employe_id", "autre_formation_id" }) })
public class EmployeAutreFormation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "employe_id")
    private Employe employe;
    @ManyToOne
    @JoinColumn(name = "autre_formation_id")
    private AutreFormation autre_formation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public AutreFormation getAutre_formation() {
        return autre_formation;
    }

    public void setAutre_formation(AutreFormation autre_formation) {
        this.autre_formation = autre_formation;
    }
}
