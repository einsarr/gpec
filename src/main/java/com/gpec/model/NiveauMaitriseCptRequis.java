package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class NiveauMaitriseCptRequis implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String libelle_maitrise;
    private int valeur_niv;

    @JsonIgnore
    @OneToMany(mappedBy = "niveauMaitriseCptRequis")
    private List<Competence> competences;
    @JsonIgnore
    @OneToMany(mappedBy = "niveauMaitriseCptAquis")
    private List<Evaluation> evaluations;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle_maitrise() {
        return libelle_maitrise;
    }

    public void setLibelle_maitrise(String libelle_maitrise) {
        this.libelle_maitrise = libelle_maitrise;
    }

    public int getValeur_niv() {
        return valeur_niv;
    }

    public void setValeur_niv(int valeur_niv) {
        this.valeur_niv = valeur_niv;
    }

    public List<Competence> getCompetences() {
        return competences;
    }

    public void setCompetences(List<Competence> competences) {
        this.competences = competences;
    }

    public List<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }
}
