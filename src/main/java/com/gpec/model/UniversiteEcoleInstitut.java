package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class UniversiteEcoleInstitut implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String code_ecole;
    @Column(unique=true)
    private String libelle_ecole;
    private int etat_ecole;

    @JsonIgnore
    @OneToMany(mappedBy = "universite_ecole_institut")
    private List<EmployeDiplomeProfessionnel> employeDiplomeProfessionnels;

    @JsonIgnore
    @OneToMany(mappedBy = "universite_ecole_institut")
    private List<EmployeDiplomeAcademique> employeDiplomeAcademiques;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode_ecole() {
        return code_ecole;
    }

    public void setCode_ecole(String code_ecole) {
        this.code_ecole = code_ecole;
    }

    public String getLibelle_ecole() {
        return libelle_ecole;
    }

    public void setLibelle_ecole(String libelle_ecole) {
        this.libelle_ecole = libelle_ecole;
    }

    public int getEtat_ecole() {
        return etat_ecole;
    }

    public void setEtat_ecole(int etat_ecole) {
        this.etat_ecole = etat_ecole;
    }

}
