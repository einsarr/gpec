package com.gpec.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "UniqueNumberAndStatus", columnNames = { "employe_id", "motif_absenc_id" }) })
public class Absence implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date_debut_abs;
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date_fin_prev;
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date_reprise;
    @ManyToOne
    @JoinColumn(name = "employe_id")
    private Employe employe;
    @ManyToOne
    @JoinColumn(name = "motif_absenc_id")
    private MotifAbsence motif_absence;
    private int etat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate_debut_abs() {
        return date_debut_abs;
    }

    public void setDate_debut_abs(Date date_debut_abs) {
        this.date_debut_abs = date_debut_abs;
    }

    public Date getDate_fin_prev() {
        return date_fin_prev;
    }

    public void setDate_fin_prev(Date date_fin_prev) {
        this.date_fin_prev = date_fin_prev;
    }

    public Date getDate_reprise() {
        return date_reprise;
    }

    public void setDate_reprise(Date date_reprise) {
        this.date_reprise = date_reprise;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public MotifAbsence getMotif_absence() {
        return motif_absence;
    }

    public void setMotif_absence(MotifAbsence motif_absence) {
        this.motif_absence = motif_absence;
    }
}
