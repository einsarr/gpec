package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Grade implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String libelle_grade;
    private int etat_grade;

    @JsonIgnore
    @OneToMany(mappedBy = "grade")
    private List<Employe> employes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle_grade() {
        return libelle_grade;
    }

    public void setLibelle_grade(String libelle_grade) {
        this.libelle_grade = libelle_grade;
    }

    public int getEtat_grade() {
        return etat_grade;
    }

    public void setEtat_grade(int etat_grade) {
        this.etat_grade = etat_grade;
    }

    public List<Employe> getEmployes() {
        return employes;
    }

    public void setEmployes(List<Employe> employes) {
        this.employes = employes;
    }
}
