package com.gpec.dao;

import com.gpec.model.EmployeDepartement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeDepartementRepository extends JpaRepository<EmployeDepartement, Long> {

//    @Query(value = "SELECT emp.*,dpt.*,empdpt.* from EmployeDepartement empdpt JOIN empdpt.employes emp" +
//            "JOIN empdpt.departements dpt")
//    public String getEmployes();



}
