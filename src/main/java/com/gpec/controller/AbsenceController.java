package com.gpec.controller;

import com.gpec.dao.AbsenceRepository;
import com.gpec.dao.EmployeRepository;
import com.gpec.dao.MotifAbsenceRepository;
import com.gpec.dao.UtilisateurRepository;
import com.gpec.model.Absence;
import com.gpec.model.Employe;
import com.gpec.model.MotifAbsence;
import com.gpec.model.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.lang.reflect.Executable;
import java.util.List;

@Controller
public class AbsenceController implements Serializable {
    @Autowired
    private AbsenceRepository absenceRepository;

    @Autowired
    private EmployeRepository employeRepository;

    @Autowired
    private MotifAbsenceRepository motifAbsenceRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @GetMapping("/absences")
    public String AbsencesPage(Model model) {
        Absence absence = new Absence();
        List<Employe> employes              = employeRepository.findAll();
        List<MotifAbsence> motifAbsences    = motifAbsenceRepository.findAll();
        List<Absence> absences              = absenceRepository.findAll();
        model.addAttribute("employes", employes);
        model.addAttribute("motifAbsences", motifAbsences);
        model.addAttribute("absence", absence);
        model.addAttribute("absences", absences);
        return "employes/absences";
    }
    @Transactional
    @PostMapping("/addAbsence")
    public String addAbsence(@ModelAttribute("absence") Absence absence) {
        Employe employe = employeRepository.findById(absence.getEmploye().getId()).get();
        boolean dec = absence.getMotif_absence().getLibelle_motif_absence().equalsIgnoreCase("Décès");
//        boolean lic = absence.getMotif_absence().getLibelle_motif_absence().equalsIgnoreCase("Décès");
        boolean dem = absence.getMotif_absence().getLibelle_motif_absence().equalsIgnoreCase("Décès");
        if(dec || dem){
            employe.setEtat(-1);
        }
        else{
            employe.setEtat(0);
        }
        Utilisateur user =  employe.getUtilisateur();
        if(user!=null){
            user.setEnabled(false);
        }
        try {
            employeRepository.save(employe);
            absenceRepository.save(absence);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/absences";
    }

    @GetMapping("/absence/{id}")
    public @ResponseBody
    Absence getAbsence(@PathVariable(name = "id") int absenceId){
        return absenceRepository.findById(absenceId).get();
    }


    @PostMapping("/removeAbsence")
    public String removeAbsence(int absenceId) {
        Absence g = absenceRepository.findById(absenceId).get();
        absenceRepository.delete(g);
        return "redirect:/absences";
    }
}
