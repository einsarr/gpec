package com.gpec.controller;

import com.gpec.dao.DiplomeProfessionnelRepository;
import com.gpec.dao.EmployeDiplomeProfRepository;
import com.gpec.dao.EmployeRepository;
import com.gpec.dao.UniversiteEcoleInstitutRepository;
import com.gpec.model.DiplomeProfessionel;
import com.gpec.model.Employe;
import com.gpec.model.EmployeDiplomeProfessionnel;
import com.gpec.model.UniversiteEcoleInstitut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class EmployeDiplomeProfessionelController implements Serializable {
    @Autowired
    private EmployeDiplomeProfRepository employeDiplomeProfRepository;
    @Autowired
    private EmployeRepository employeRepository;
    @Autowired
    private DiplomeProfessionnelRepository diplomeProfessionnelRepository;

    @Autowired
    private UniversiteEcoleInstitutRepository universiteEcoleInstitutRepository;

    @GetMapping("/employe_dipl_profs")
    public String employediplomeprofessionnelPage(Model model) {
        EmployeDiplomeProfessionnel employe_diplome_professionnel           = new EmployeDiplomeProfessionnel();
        List<UniversiteEcoleInstitut> universiteEcoleInstituts      = universiteEcoleInstitutRepository.findAll();
        List<Employe> employes                                              = employeRepository.findAll();
        List<DiplomeProfessionel> diplomeProfessionels                      = diplomeProfessionnelRepository.findAll();
        List<EmployeDiplomeProfessionnel> employe_diplome_professionnels    = employeDiplomeProfRepository.findAll();
        model.addAttribute("universite_ecole_instituts", universiteEcoleInstituts);
        model.addAttribute("employes", employes);
        model.addAttribute("diplome_professionnels", diplomeProfessionels);
        model.addAttribute("employe_diplome_professionnel", employe_diplome_professionnel);
        model.addAttribute("employe_diplome_professionnels", employe_diplome_professionnels);
        return "employes/employe_diplome_professionnels";
    }
    @PostMapping("/addEmployeDiplomeProfessionnel")
    public String adddiploprof(@ModelAttribute("employe_diplome_professionnel") EmployeDiplomeProfessionnel employe_diplome_professionnel) {
        try {
            employeDiplomeProfRepository.save(employe_diplome_professionnel);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/employe_dipl_profs";
    }

    @GetMapping("/employe_diplome_professionnel/{id}")
    public @ResponseBody
    EmployeDiplomeProfessionnel getEmployeDiplomeProfessionnel(@PathVariable(name = "id") int employe_diplome_professionnelId){
        return employeDiplomeProfRepository.findById(employe_diplome_professionnelId).get();
    }


    @PostMapping("/removeEmployeDiplomeProfessionnel")
    public String removeEmployeDiplomeProfessionnel(int employe_diplome_professionnelId) {
        EmployeDiplomeProfessionnel g = employeDiplomeProfRepository.findById(employe_diplome_professionnelId).get();
        employeDiplomeProfRepository.delete(g);
        return "redirect:/employe_dipl_profs";
    }
}
