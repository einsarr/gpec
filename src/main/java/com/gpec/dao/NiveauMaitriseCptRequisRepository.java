package com.gpec.dao;

import com.gpec.model.NiveauMaitriseCptRequis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NiveauMaitriseCptRequisRepository extends JpaRepository<NiveauMaitriseCptRequis, Integer> {
}
