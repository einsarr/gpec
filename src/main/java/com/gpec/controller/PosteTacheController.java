package com.gpec.controller;

import com.gpec.dao.PosteRepository;
import com.gpec.dao.PosteTacheRepository;
import com.gpec.dao.TacheRepository;
import com.gpec.model.Poste;
import com.gpec.model.PosteTache;
import com.gpec.model.Tache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
@PreAuthorize("hasAuthority('ROLE_DIRECTEUR')")
public class PosteTacheController implements Serializable {
    @Autowired
    private PosteTacheRepository posteTacheRepository;

    @Autowired
    private PosteRepository posteRepository;

    @Autowired
    private TacheRepository tacheRepository;

    @GetMapping("/poste_taches")
    public String Poste_tachePage(Model model) {
        PosteTache posteTache           = new PosteTache();
        List<Poste> postes              = posteRepository.findAll();
        List<Tache> taches              = tacheRepository.findAll();
        List<PosteTache> poste_taches   = posteTacheRepository.findAll();
        model.addAttribute("poste_tache", posteTache);
        model.addAttribute("postes", postes);
        model.addAttribute("taches", taches);
        model.addAttribute("poste_taches", poste_taches);
        return "parametrage/poste_taches";
    }
    @PostMapping("/addPosteTache")
    public String addPosteTache(@ModelAttribute("poste_tache") PosteTache poste_tache) {
        try {
            posteTacheRepository.save(poste_tache);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/poste_taches";
    }

    @GetMapping("/poste_tache/{id}")
    public @ResponseBody
    PosteTache getPosteTache(@PathVariable(name = "id") int activiteId){
        return posteTacheRepository.findById(activiteId).get();
    }


    @PostMapping("/removePosteTache")
    public String removePosteTache(int posteTacheId) {
        PosteTache g = posteTacheRepository.findById(posteTacheId).get();
        posteTacheRepository.delete(g);
        return "redirect:/poste_taches";
    }
}
