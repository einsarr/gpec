package com.gpec.service;

import com.gpec.model.Poste;
import com.gpec.model.PosteCompetence;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.List;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class FichePoste {

    public static ByteArrayInputStream employeesReport(Poste poste) {
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Font fontTitle = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        fontTitle.setSize(18);
        fontTitle.setColor(Color.RED);
        Paragraph paragraph = new Paragraph("Fiche du poste :"+poste.getLibelle_poste(), fontTitle);
        paragraph.setAlignment(Paragraph.ALIGN_CENTER);

        Font fontParagraph = FontFactory.getFont(FontFactory.HELVETICA);
        fontParagraph.setSize(12);
        PdfPCell cell;
        PdfPCell cell1 = null;
        try {

            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(80);
            table.setWidths(new int[] { 4, 4, 4 });

            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
            Font headFont1 = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

            PdfPCell hcell;
            PdfPCell hcell1;
            hcell = new PdfPCell(new Phrase("Code du poste", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Libéllé", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell1 = new PdfPCell(new Phrase("Compétences", headFont1));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell1);

                cell = new PdfPCell(new Phrase(poste.getCode_poste()));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(poste.getLibelle_poste()));
                cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                for(PosteCompetence cpt : poste.getPoste_competences()){
                    cell1 = new PdfPCell(new Phrase(cpt.getCompetence().getLibelle_competence()));
                    cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell1);
                }

//                cell = new PdfPCell(new Phrase(String.valueOf(poste.getEtat_poste())));
//                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                cell.setPaddingRight(5);
//                table.addCell(cell);
            PdfWriter.getInstance(document, out);
            document.open();
            document.add(paragraph);
            document.add(table);

            document.close();

        } catch (DocumentException ex) {

        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    public void export(HttpServletResponse response,Poste poste) throws IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();
        Font fontTitle = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        fontTitle.setSize(18);
        fontTitle.setColor(Color.RED);

        Paragraph paragraph = new Paragraph("Fiche du poste :"+poste.getLibelle_poste(), fontTitle);
        paragraph.setAlignment(Paragraph.ALIGN_CENTER);

        Font fontParagraph = FontFactory.getFont(FontFactory.HELVETICA);
        fontParagraph.setSize(12);

        Paragraph paragraph2 = new Paragraph("This is a paragraph.", fontParagraph);
        paragraph2.setAlignment(Paragraph.ALIGN_LEFT);
        List lists = new List();
        for(PosteCompetence cpt : poste.getPoste_competences()){
            if(cpt.getCompetence().getCategorie().equals("Savoir")){
                //lists.add(cpt.getCompetence().getLibelle_competence());
                paragraph2.add(cpt.getCompetence().getLibelle_competence());
            }
        }
        document.add(lists);
        document.add(paragraph);
        document.add(paragraph2);
        document.close();
    }

}
