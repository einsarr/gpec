$(document).ready(function () {
    $.ajax({
        url:'repartition_competences',
        dataType:"json",
        success:function (result) {
            repartitionCompetences(result[0],result[1]);
        }
    });
});

function repartitionCompetences(cpt,valeurs) {
    Highcharts.chart('repartition_competences', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Répartition des compétences'
        },
        xAxis: {
            categories: cpt
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Compétence',
            data: valeurs
        }]
    });

}