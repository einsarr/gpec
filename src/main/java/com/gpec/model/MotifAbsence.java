package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class MotifAbsence implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String libelle_motif_absence;
    @Column(unique=true)
    private String nature_absence;
    private int deductible;

    @JsonIgnore
    @OneToMany(mappedBy = "motif_absence")
    private List<Absence> absences;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Absence> getAbsences() {
        return absences;
    }

    public void setAbsences(List<Absence> absences) {
        this.absences = absences;
    }

    public String getLibelle_motif_absence() {
        return libelle_motif_absence;
    }

    public void setLibelle_motif_absence(String libelle_motif_absence) {
        this.libelle_motif_absence = libelle_motif_absence;
    }

    public String getNature_absence() {
        return nature_absence;
    }

    public void setNature_absence(String nature_absence) {
        this.nature_absence = nature_absence;
    }

    public int getDeductible() {
        return deductible;
    }

    public void setDeductible(int deductible) {
        this.deductible = deductible;
    }
}
