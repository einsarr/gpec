package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Entity
public class Activite implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique=true)
    private String libelle_activite;
    @Column(unique=true)
    private String code_activite;
    private int etat_activite;

    @JsonIgnore
    @OneToMany(mappedBy = "activite")
    private List<Tache> taches;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Tache> getTaches() {
        return taches;
    }

    public void setTaches(List<Tache> taches) {
        this.taches = taches;
    }

    public String getLibelle_activite() {
        return libelle_activite;
    }

    public void setLibelle_activite(String libelle_activite) {
        this.libelle_activite = libelle_activite;
    }

    public int getEtat_activite() {
        return etat_activite;
    }

    public void setEtat_activite(int etat_activite) {
        this.etat_activite = etat_activite;
    }

    public String getCode_activite() {
        return code_activite;
    }

    public void setCode_activite(String code_activite) {
        this.code_activite = code_activite;
    }
}
