package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
public class Poste implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique=true)
    private String code_poste;
    @Column(unique=true)
    @Size(min = 4,max = 250)
    private String libelle_poste;
    private int etat_poste;
    @JsonIgnore
    @OneToMany(mappedBy = "poste")
    private List<PosteCompetence> poste_competences;
    @JsonIgnore
    @OneToMany(mappedBy = "poste")
    private List<EmployePoste> poste_employes;
    @JsonIgnore
    @OneToMany(mappedBy = "poste")
    private List<PosteMission> poste_missions;
    @JsonIgnore
    @OneToMany(mappedBy = "poste")
    private List<PosteTache> poste_taches;
    @ManyToOne
    @JoinColumn(name = "exigence_id")
    private ExigencePoste exigencePoste;
    @ManyToOne
    @JoinColumn(name = "emploi_type_id")
    private EmploiType emploiType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode_poste() {
        return code_poste;
    }

    public void setCode_poste(String code_poste) {
        this.code_poste = code_poste;
    }

    public String getLibelle_poste() {
        return libelle_poste;
    }

    public void setLibelle_poste(String libelle_poste) {
        this.libelle_poste = libelle_poste;
    }

    public int getEtat_poste() {
        return etat_poste;
    }

    public void setEtat_poste(int etat_poste) {
        this.etat_poste = etat_poste;
    }

    public List<PosteCompetence> getPoste_competences() {
        return poste_competences;
    }

    public void setPoste_competences(List<PosteCompetence> poste_competences) {
        this.poste_competences = poste_competences;
    }

    public List<EmployePoste> getPoste_employes() {
        return poste_employes;
    }

    public void setPoste_employes(List<EmployePoste> poste_employes) {
        this.poste_employes = poste_employes;
    }

    public List<PosteMission> getPoste_missions() {
        return poste_missions;
    }

    public void setPoste_missions(List<PosteMission> poste_missions) {
        this.poste_missions = poste_missions;
    }

    public List<PosteTache> getPoste_taches() {
        return poste_taches;
    }

    public void setPoste_taches(List<PosteTache> poste_taches) {
        this.poste_taches = poste_taches;
    }

    public ExigencePoste getExigencePoste() {
        return exigencePoste;
    }

    public void setExigencePoste(ExigencePoste exigencePoste) {
        this.exigencePoste = exigencePoste;
    }

    public EmploiType getEmploiType() {
        return emploiType;
    }

    public void setEmploiType(EmploiType emploiType) {
        this.emploiType = emploiType;
    }
}
