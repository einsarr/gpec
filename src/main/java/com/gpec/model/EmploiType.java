package com.gpec.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class EmploiType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String libelle_emploi_type;
    private int etat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle_emploi_type() {
        return libelle_emploi_type;
    }

    public void setLibelle_emploi_type(String libelle_emploi_type) {
        this.libelle_emploi_type = libelle_emploi_type;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
}
