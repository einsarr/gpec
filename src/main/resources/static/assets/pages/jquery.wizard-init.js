/**
* Theme: Montran Admin Template
* Author: Coderthemes
* Form wizard page
*/

!function($) {
    "use strict";

    var FormWizard = function() {};

    FormWizard.prototype.createBasic = function($form_container) {
        $form_container.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onFinished: function (event, currentIndex) {
                var a =$('#moussa').val();
                alert(a);
            }
        });
        return $form_container;
    },
    //creates form with validation
    FormWizard.prototype.createValidatorForm = function($form_container) {
        $form_container.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.after(error);
            }
        });
        $form_container.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex) {
                $form_container.validate().settings.ignore = ":disabled,:hidden";
                return $form_container.valid();
            },
            onFinishing: function (event, currentIndex) {
                $form_container.validate().settings.ignore = ":disabled";
                return $form_container.valid();
            },
            onFinished: function (event, currentIndex) {
                /*var ok = $('#niv').val();
                alert(ok);*/
                // $("#nivsr > option").each(function() {
                //     alert(this.value);
                // });

                var url = window.location;
                var data={}
                $.each($(".nivsr > option:selected"),function() {
                    data = this.value.split('-');
                    //alert(data);return;
                    var Obj = data[0]+"-"+data[1]+"-"+data[2];
                    //alert(Obj);return;
                    $.ajax({
                        url: "addEvaluation",
                        type: "POST",
                        dataType: "json",
                        data: Obj,
                        contentType: "application/json;charset=utf-8",
                        success: function (result) {
                            window.location.replace("employes");
                        },
                        error: function (errormessage) {
                            alert(errormessage.responseText);
                        }
                    });
                });
                $.Notification.autoHideNotify('success', 'bottom right', 'Alerte', 'Evaluation enregistrée avec succes');
            }
        });

        return $form_container;
    },
    //creates vertical form
    FormWizard.prototype.createVertical = function($form_container) {
        $form_container.steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "fade",
            stepsOrientation: "vertical"
        });
        return $form_container;
    },
    FormWizard.prototype.init = function() {
        //initialzing various forms

        //basic form
        this.createBasic($("#basic-form"));

        //form with validation
        this.createValidatorForm($("#wizard-validation-form"));

        //vertical form
        this.createVertical($("#wizard-vertical"));
    },
    //init
    $.FormWizard = new FormWizard, $.FormWizard.Constructor = FormWizard
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.FormWizard.init()
}(window.jQuery);