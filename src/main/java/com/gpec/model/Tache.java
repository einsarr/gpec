package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Tache implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String libelle_tache;
    private int etat_tache;

    @ManyToOne
    @JoinColumn(name = "activite_id")
    private Activite activite;

    @JsonIgnore
    @OneToMany(mappedBy = "tache")
    private List<PosteTache> poste_taches;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle_tache() {
        return libelle_tache;
    }

    public void setLibelle_tache(String libelle_tache) {
        this.libelle_tache = libelle_tache;
    }

    public int getEtat_tache() {
        return etat_tache;
    }

    public void setEtat_tache(int etat_tache) {
        this.etat_tache = etat_tache;
    }

    public List<PosteTache> getPoste_taches() {
        return poste_taches;
    }

    public void setPoste_taches(List<PosteTache> poste_taches) {
        this.poste_taches = poste_taches;
    }

    public Activite getActivite() {
        return activite;
    }

    public void setActivite(Activite activite) {
        this.activite = activite;
    }
}
