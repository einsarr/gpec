package com.gpec.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class AutreFormation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String code_autref;
    @Column(unique=true)
    private String libelle_autref;
    @Column(unique=true)
    private String description_autref;
    private int etat_autref;

    @OneToMany(mappedBy = "autre_formation")
    private List<EmployeAutreFormation> employe_autreFormations;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode_autref() {
        return code_autref;
    }

    public void setCode_autref(String code_autref) {
        this.code_autref = code_autref;
    }

    public String getLibelle_autref() {
        return libelle_autref;
    }

    public void setLibelle_autref(String libelle_autref) {
        this.libelle_autref = libelle_autref;
    }

    public String getDescription_autref() {
        return description_autref;
    }

    public void setDescription_autref(String description_autref) {
        this.description_autref = description_autref;
    }

    public int getEtat_autref() {
        return etat_autref;
    }

    public void setEtat_autref(int etat_autref) {
        this.etat_autref = etat_autref;
    }

    public List<EmployeAutreFormation> getEmploye_autreFormations() {
        return employe_autreFormations;
    }

    public void setEmploye_autreFormations(List<EmployeAutreFormation> employe_autreFormations) {
        this.employe_autreFormations = employe_autreFormations;
    }
}
