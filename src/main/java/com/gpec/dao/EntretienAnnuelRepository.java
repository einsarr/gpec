package com.gpec.dao;

import com.gpec.model.EntretienAnnuel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EntretienAnnuelRepository extends JpaRepository<EntretienAnnuel, Integer> {
}
