package com.gpec.dao;

import com.gpec.model.EmployeDiplomeAcademique;
import com.gpec.model.Poste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeDiplomeAcadRepository extends JpaRepository<EmployeDiplomeAcademique, Integer> {

    @Query(value = "SELECT * from poste p join employe_poste ep " +
            "on ep.poste_id=p.id join employe e on ep.employe_id=e.id WHERE e.etat=1 AND e.id=:employeId",nativeQuery = true)
    public Poste findPosteByEmploye(@Param("employeId")Long employeId);

}
