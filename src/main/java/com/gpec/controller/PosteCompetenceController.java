package com.gpec.controller;

import com.gpec.dao.CompetenceRepository;
import com.gpec.dao.PosteCompetenceRepository;
import com.gpec.dao.PosteRepository;
import com.gpec.model.Competence;
import com.gpec.model.Poste;
import com.gpec.model.PosteCompetence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class PosteCompetenceController implements Serializable {
    @Autowired
    private PosteCompetenceRepository posteCompetenceRepository;

    @Autowired
    private PosteRepository posteRepository;

    @Autowired
    private CompetenceRepository competenceRepository;

    @GetMapping("/poste_competences")
    public String PosteCompetencesPage(Model model) {
        PosteCompetence poste_competence        = new PosteCompetence();
        List<Poste> postes                      = posteRepository.findAll();
        List<Competence> competences            = competenceRepository.findAll();
        List<PosteCompetence> poste_competences = posteCompetenceRepository.findAll();
        model.addAttribute("poste_competence", poste_competence);
        model.addAttribute("postes", postes);
        model.addAttribute("competences", competences);
        model.addAttribute("poste_competences", poste_competences);
        return "parametrage/poste_competences";
    }
    @PostMapping("/addPosteCompetence")
    public String addPosteCompetence(@ModelAttribute("poste_competence") PosteCompetence poste_competence) {
        try {
            posteCompetenceRepository.save(poste_competence);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/poste_competences";
    }

    @GetMapping("/poste_competence/{id}")
    public @ResponseBody
    PosteCompetence getPosteCompetence(@PathVariable(name = "id") int poste_competenceId){
        return posteCompetenceRepository.findById(poste_competenceId).get();
    }


    @PostMapping("/removePosteCompetence")
    public String removePosteCompetence(int poste_competenceId) {
        PosteCompetence g = posteCompetenceRepository.findById(poste_competenceId).get();
        posteCompetenceRepository.delete(g);
        return "redirect:/poste_competences";
    }
}
