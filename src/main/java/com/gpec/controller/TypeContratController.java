package com.gpec.controller;

import com.gpec.dao.TypeContratRepository;
import com.gpec.model.TypeContrat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class TypeContratController implements Serializable {
    @Autowired
    private TypeContratRepository type_contratRepository;

    @GetMapping("/type_contrats")
    public String TypeContratsPage(Model model) {
        TypeContrat type_contrat = new TypeContrat();
        model.addAttribute("type_contrat", type_contrat);
        List<TypeContrat> type_contrats = type_contratRepository.findAll();
        model.addAttribute("type_contrats", type_contrats);
        return "employes/type_contrats";
    }
    @PostMapping("/addTypeContrat")
    public String addTypeContrat(@ModelAttribute("type_contrat") TypeContrat type_contrat) {
        try {
            type_contratRepository.save(type_contrat);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/type_contrats";
    }

    @GetMapping("/type_contrat/{id}")
    public @ResponseBody
    TypeContrat getTypeContrat(@PathVariable(name = "id") int type_contratId){
        return type_contratRepository.findById(type_contratId).get();
    }


    @PostMapping("/removeTypeContrat")
    public String removeTypeContrat(int type_contratId) {
        TypeContrat g = type_contratRepository.findById(type_contratId).get();
        type_contratRepository.delete(g);
        return "redirect:/type_contrats";
    }
}
