package com.gpec.controller;

import com.gpec.dao.ExigencePosteRepository;
import com.gpec.model.ExigencePoste;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class ExigencePosteController implements Serializable {
    @Autowired
    private ExigencePosteRepository exigencePosteRepository;

    @GetMapping("/exigence_postes")
    public String corpsPage(Model model) {
        ExigencePoste exigence_poste = new ExigencePoste();
        model.addAttribute("exigence_poste", exigence_poste);
        List<ExigencePoste> exigence_postes = exigencePosteRepository.findAll();
        model.addAttribute("exigence_postes", exigence_postes);
        return "parametrage/exigence_postes";
    }
    @PostMapping("/addExigencePoste")
    public String addcorps(@ModelAttribute("exigence_poste") ExigencePoste exigence_poste) {
        try {
            exigencePosteRepository.save(exigence_poste);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/exigence_postes";
    }

    @GetMapping("/exigence_poste/{id}")
    public @ResponseBody
    ExigencePoste getExigencePoste(@PathVariable(name = "id") int exigence_posteId){
        return exigencePosteRepository.findById(exigence_posteId).get();
    }


    @PostMapping("/removeExigencePoste")
    public String removeExigencePoste(int exigence_posteId) {
        ExigencePoste g = exigencePosteRepository.findById(exigence_posteId).get();
        exigencePosteRepository.delete(g);
        return "redirect:/exigence_postes";
    }
}
