package com.gpec.dao;

import com.gpec.model.EmployePoste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployePosteRepository extends JpaRepository<EmployePoste, Integer> {
}
