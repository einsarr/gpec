package com.gpec.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Annee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private int code_annee;
    @Column(unique=true)
    private String libelle_annee;
    private int etat_annee;

    @JsonIgnore
    @OneToOne(mappedBy = "annee")
    private EffectifAnnee EffectifAnnee;

    @JsonIgnore
    @OneToMany(mappedBy = "annee")
    private List<Evaluation> evaluations;

    @JsonIgnore
    @OneToMany(mappedBy = "annee")
    private List<EntretienAnnuel> entretienAnnuels;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode_annee() {
        return code_annee;
    }

    public void setCode_annee(int code_annee) {
        this.code_annee = code_annee;
    }

    public String getLibelle_annee() {
        return libelle_annee;
    }

    public void setLibelle_annee(String libelle_annee) {
        this.libelle_annee = libelle_annee;
    }

    public int getEtat_annee() {
        return etat_annee;
    }

    public void setEtat_annee(int etat_annee) {
        this.etat_annee = etat_annee;
    }

    public EffectifAnnee getEffectifAnnee() {
        return EffectifAnnee;
    }

    public void setEffectifAnnee(EffectifAnnee effectifAnnee) {
        EffectifAnnee = effectifAnnee;
    }

    public List<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }

    public List<EntretienAnnuel> getEntretienAnnuels() {
        return entretienAnnuels;
    }

    public void setEntretienAnnuels(List<EntretienAnnuel> entretienAnnuels) {
        this.entretienAnnuels = entretienAnnuels;
    }
}
