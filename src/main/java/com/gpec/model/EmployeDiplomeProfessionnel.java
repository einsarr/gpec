package com.gpec.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "UniqueNumberAndStatus", columnNames = { "employe_id", "diplome_prof_id" }) })
public class EmployeDiplomeProfessionnel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "employe_id")
    private Employe employe;
    @ManyToOne
    @JoinColumn(name = "diplome_prof_id")
    private DiplomeProfessionel diplome_professionnel;
    @ManyToOne
    @JoinColumn(name = "ecole_id")
    private UniversiteEcoleInstitut universite_ecole_institut;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date_dipl_prof;

    public UniversiteEcoleInstitut getUniversite_ecole_institut() {
        return universite_ecole_institut;
    }

    public void setUniversite_ecole_institut(UniversiteEcoleInstitut universite_ecole_institut) {
        this.universite_ecole_institut = universite_ecole_institut;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public DiplomeProfessionel getDiplome_professionnel() {
        return diplome_professionnel;
    }

    public void setDiplome_professionnel(DiplomeProfessionel diplome_professionnel) {
        this.diplome_professionnel = diplome_professionnel;
    }

    public Date getDate_dipl_prof() {
        return date_dipl_prof;
    }

    public void setDate_dipl_prof(Date date_dipl_prof) {
        this.date_dipl_prof = date_dipl_prof;
    }

}
