package com.gpec.controller;

import com.gpec.dao.DiplomeAcademiqueRepository;
import com.gpec.dao.UniversiteEcoleInstitutRepository;
import com.gpec.model.DiplomeAcademique;
import com.gpec.model.UniversiteEcoleInstitut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.EOFException;
import java.io.Serializable;
import java.util.List;

@Controller
public class DiplomeAcademiqueController implements Serializable {
    @Autowired
    private DiplomeAcademiqueRepository diplome_academiqueRepository;

    @GetMapping("/diplome_academiques")
    public String diplomeacademiquePage(Model model) {
        DiplomeAcademique diplome_academique = new DiplomeAcademique();
        model.addAttribute("diplome_academique", diplome_academique);
        List<DiplomeAcademique> diplome_academiques = diplome_academiqueRepository.findAll();
        model.addAttribute("diplome_academiques", diplome_academiques);
        return "employes/diplome_academiques";
    }
    @PostMapping("/addDiplomeAcademique")
    public String addcorps(@ModelAttribute("diplome_academique") DiplomeAcademique diplome_academique) {
        try {
            diplome_academiqueRepository.save(diplome_academique);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/diplome_academiques";
    }

    @GetMapping("/diplome_academique/{id}")
    public @ResponseBody
    DiplomeAcademique getDiplomeAcademique(@PathVariable(name = "id") int diplome_academiqueId){
        return diplome_academiqueRepository.findById(diplome_academiqueId).get();
    }

    @PostMapping("/removeDiplomeAcademique")
    public String removeDiplomeAcademique(int diplome_academiqueId) {
        DiplomeAcademique g = diplome_academiqueRepository.findById(diplome_academiqueId).get();
        diplome_academiqueRepository.delete(g);
        return "redirect:/diplome_academiques";
    }
}
