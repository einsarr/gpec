package com.gpec.controller;

import com.gpec.dao.UniversiteEcoleInstitutRepository;
import com.gpec.model.UniversiteEcoleInstitut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class UniveristeEcoleInstitutController implements Serializable {
    @Autowired
    private UniversiteEcoleInstitutRepository universite_ecole_institutRepository;

    @GetMapping("/ecoles")
    public String ecolePage(Model model) {
        UniversiteEcoleInstitut universite_ecole_institut = new UniversiteEcoleInstitut();
        model.addAttribute("universite_ecole_institut", universite_ecole_institut);
        List<UniversiteEcoleInstitut> universite_ecole_instituts = universite_ecole_institutRepository.findAll();
        model.addAttribute("ecoles", universite_ecole_instituts);
        return "employes/ecoles";
    }
    @PostMapping("/addEcole")
    public String addEcole(@ModelAttribute("universite_ecole_institut") UniversiteEcoleInstitut universite_ecole_institut) {
        try {
            universite_ecole_institutRepository.save(universite_ecole_institut);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/ecoles";
    }

    @GetMapping("/ecole/{id}")
    public @ResponseBody
    UniversiteEcoleInstitut getEcole(@PathVariable(name = "id") int ecoleId){
        return universite_ecole_institutRepository.findById(ecoleId).get();
    }


    @PostMapping("/removeEcole")
    public String removeEcole(int ecoleId) {
        UniversiteEcoleInstitut g = universite_ecole_institutRepository.findById(ecoleId).get();
        universite_ecole_institutRepository.delete(g);
        return "redirect:/ecoles";
    }
}
