package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Potentiel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private int valeur_potentiel;
    @Column(unique=true)
    private String libelle_potentiel;
    private int etat_potentiel;

    @JsonIgnore
    @OneToMany(mappedBy = "potentiel")
    private List<EntretienAnnuel> entretienAnnuels;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValeur_potentiel() {
        return valeur_potentiel;
    }

    public void setValeur_potentiel(int valeur_potentiel) {
        this.valeur_potentiel = valeur_potentiel;
    }

    public String getLibelle_potentiel() {
        return libelle_potentiel;
    }

    public void setLibelle_potentiel(String libelle_potentiel) {
        this.libelle_potentiel = libelle_potentiel;
    }

    public int getEtat_potentiel() {
        return etat_potentiel;
    }

    public void setEtat_potentiel(int etat_potentiel) {
        this.etat_potentiel = etat_potentiel;
    }
}
