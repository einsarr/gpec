#FROM tomcat:9
#RUN rm -rf /usr/local/tomcat/webapps/*
#EXPOSE 8002
#COPY ./target/gpecdrhmen.war /usr/local/tomcat/webapps/ROOT.war
#CMD ["catalina.sh","run"]

 FROM openjdk:11
 EXPOSE 8002
 ADD target/gpecdrhmen.jar gpecdrhmen.jar
 ENTRYPOINT ["java","-jar","/gpecdrhmen.jar"]