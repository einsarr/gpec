package com.gpec.controller;

import com.gpec.dao.AnneeRepository;
import com.gpec.model.Annee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class AnneeController implements Serializable {
    @Autowired
    private AnneeRepository anneeRepository;

    @GetMapping("/annees")
    public String AnneesPage(Model model) {
        Annee annee = new Annee();
        model.addAttribute("annee", annee);
        List<Annee> annees = anneeRepository.findAll();
        model.addAttribute("annees", annees);
        return "parametrage/annees";
    }
    @PostMapping("/addAnnee")
    public String addAnnee(@ModelAttribute("annee") Annee annee) {
        try{
            anneeRepository.save(annee);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/annees";
    }

    @GetMapping("/annee/{id}")
    public @ResponseBody
    Annee getAnnee(@PathVariable(name = "id") int anneeId){
        return anneeRepository.findById(anneeId).get();
    }


    @PostMapping("/removeAnnee")
    public String removeAnnee(int anneeId) {
        Annee g = anneeRepository.findById(anneeId).get();
        anneeRepository.delete(g);
        return "redirect:/annees";
    }
}
