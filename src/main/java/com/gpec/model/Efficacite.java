package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Efficacite implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private int valeur_efficacite;
    @Column(unique=true)
    private String libelle_efficacite;
    private int etat_efficacite;
    @JsonIgnore
    @OneToMany(mappedBy = "efficacite")
    private List<EntretienAnnuel> entretienAnnuels;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValeur_efficacite() {
        return valeur_efficacite;
    }

    public void setValeur_efficacite(int valeur_efficacite) {
        this.valeur_efficacite = valeur_efficacite;
    }

    public String getLibelle_efficacite() {
        return libelle_efficacite;
    }

    public void setLibelle_efficacite(String libelle_efficacite) {
        this.libelle_efficacite = libelle_efficacite;
    }

    public int getEtat_efficacite() {
        return etat_efficacite;
    }

    public void setEtat_efficacite(int etat_efficacite) {
        this.etat_efficacite = etat_efficacite;
    }
}
