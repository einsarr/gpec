package com.gpec.controller;

import com.gpec.dao.*;
import com.gpec.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Controller
@PreAuthorize("hasAnyRole('ROLE_DIRECTEUR','ROLE_MANAGER')")
public class EmployeController implements Serializable {
    @Autowired
    private EmployeRepository employeRepository;

    @Autowired
    private CorpsRepository corpsRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private GradeRepository gradeRepository;

    @Autowired
    private EffectifAnneeRepository effectifAnneeRepository;

    @GetMapping("/employes")
    public String employePage(Model model, HttpServletRequest request) {
        Employe emp = new Employe();
        List<Employe> employes = new ArrayList<>();
        if(request.isUserInRole("ROLE_DIRECTEUR")==true || request.isUserInRole("ROLE_ADMIN")==true){
            employes = employeRepository.findEmployeEtatActif();
        }else{
            Utilisateur user =(Utilisateur) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            employes = employeRepository.employeByDepartement(user.getDepartement().getId());
        }
        List<Corps> corps = corpsRepository.findAll();
        List<Grade> grades = gradeRepository.findAll();
        model.addAttribute("employe", emp);
        model.addAttribute("employes", employes);
        model.addAttribute("corps", corps);
        model.addAttribute("grades", grades);
        return "employes/employes";
    }
    @PostMapping("/addEmploye")
    public String addEmploye(@ModelAttribute("employe") Employe employe){
        if(employe.getId()!=null){
            employe.setEtat(1);
        }else{
            EffectifAnnee effectifAnnee = effectifAnneeRepository.findEffectifAnneeByEtat();
            effectifAnnee.setEffectif_entrant(effectifAnnee.getEffectif_entrant()+1);
            try {
                effectifAnneeRepository.save(effectifAnnee);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        try {
            employeRepository.save(employe);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/employes";
    }

    @GetMapping("/employe/{id}")
    public @ResponseBody
    Employe getEmploye(@PathVariable(name = "id") Long employeId){
        return employeRepository.findById(employeId).get();
    }

    @PostMapping("/removeEmploye")
    public String removeEmploye(Long employeId) {
        Employe c = employeRepository.findById(employeId).get();
        EffectifAnnee effectifAnnee = effectifAnneeRepository.findEffectifAnneeByEtat();
        effectifAnnee.setEffectif_entrant(effectifAnnee.getEffectif_entrant()-1);
        effectifAnneeRepository.save(effectifAnnee);
        employeRepository.delete(c);
        return "redirect:/employes";
    }

    @RequestMapping("/getProfileEmploye")
    public String getProfilEmploye(@RequestParam(name = "id") Long id,Model model)
    {
        Employe employe = employeRepository.getEmployeById(id);
        model.addAttribute("employe", employe);
        return "employe/profile";
    }

    @PostMapping("/archiveEmploye")
    public String archiveEmploye(Long employeId) {
        Employe e = employeRepository.findById(employeId).get();
        Utilisateur u = utilisateurRepository.findByUsername(e.getUtilisateur().getUsername());
        e.setEtat(-1);
        u.setEnabled(false);
        utilisateurRepository.save(u);
        employeRepository.save(e);
        return "redirect:/employes";
    }

}
