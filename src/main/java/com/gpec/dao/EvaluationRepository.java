package com.gpec.dao;
import com.gpec.model.Evaluation;
import com.gpec.model.Poste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface EvaluationRepository extends JpaRepository<Evaluation, Long> {

    @Query(value = "SELECT * from evaluation ev join niveau_maitrise_cpt_requis c on c.id=ev.niveau_maitrise_id" +
            " join employe e on e.id=ev.employe_id join competence cpt on cpt.id=ev.competence_id WHERE cpt.est_matrice=1 AND e.id=:employeId",nativeQuery = true)
    List<Evaluation> getEvaluationsMatrice(@Param("employeId")Long employeId);


    @Query(value = "SELECT * from evaluation ev join " +
            " employe e on e.id=ev.employe_id WHERE e.etat=1 AND e.id=:employeId",nativeQuery = true)
    List<Evaluation> evaluationsByEmploye(@Param("employeId")Long employeId);
}
