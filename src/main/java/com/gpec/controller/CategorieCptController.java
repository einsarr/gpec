package com.gpec.controller;

import com.gpec.dao.CategorieCompetenceRepository;
import com.gpec.model.CategorieCompetence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class CategorieCptController implements Serializable {
    @Autowired
    private CategorieCompetenceRepository categorieCompetenceRepository;

    
    @GetMapping("/categorie_competences")
    public String CategCptPage(Model model) {
        CategorieCompetence categorie_competence = new CategorieCompetence();
        model.addAttribute("categorie_competence", categorie_competence);
        List<CategorieCompetence> categorie_competences = categorieCompetenceRepository.findAll();
        model.addAttribute("categorie_competences", categorie_competences);
        return "parametrage/categorie_competences";
    }
    @PostMapping("/addCategorie_competence")
    public String addCategCpt(@ModelAttribute("categorie_competence") CategorieCompetence categorie_competence) {
        try {
            categorieCompetenceRepository.save(categorie_competence);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/categorie_competences";
    }

    @GetMapping("/categorie_competence/{id}")
    public @ResponseBody
    CategorieCompetence getCategorie_competence(@PathVariable(name = "id") int categorie_competenceId){
        return categorieCompetenceRepository.findById(categorie_competenceId).get();
    }


    @PostMapping("/removeCategorie_competence")
    public String removeCategorie_competence(int categorie_competenceId) {
        CategorieCompetence g = categorieCompetenceRepository.findById(categorie_competenceId).get();
        categorieCompetenceRepository.delete(g);
        return "redirect:/categorie_competences";
    }
}
