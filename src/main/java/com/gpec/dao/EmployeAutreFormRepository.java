package com.gpec.dao;

import com.gpec.model.EmployeAutreFormation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeAutreFormRepository extends JpaRepository<EmployeAutreFormation, Integer> {
}
