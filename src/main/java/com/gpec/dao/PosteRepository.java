package com.gpec.dao;

import com.gpec.model.Employe;
import com.gpec.model.Poste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PosteRepository extends JpaRepository<Poste, Long> {
    /**
     * Retourner les détails du poste de l'employé
     * @param employeId
     * @return
     */
    @Query(value = "SELECT * from poste p join employe_poste ep " +
            "on ep.poste_id=p.id join employe e on ep.employe_id=e.id WHERE e.etat=1 AND e.id=:employeId",nativeQuery = true)
    Poste findPosteByEmploye(@Param("employeId")Long employeId);

    @Query(value = "SELECT * FROM poste p WHERE p.id=:posteId",nativeQuery = true)
    Poste findPosteById(@Param("posteId")Long posteId);

    @Query(value = "SELECT max(id) from poste",nativeQuery = true)
    long getLastPoste();

}
