package com.gpec.controller;
import com.gpec.dao.*;
import com.gpec.model.*;
import com.gpec.service.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
public class StatistiqueReportController {
    @Autowired
    private EmployeRepository employeRepository;
    @Autowired
    private EffectifAnneeRepository effectifAnneeRepository;
    @Autowired
    private ContratRepository contratRepository;
    @Autowired
    private CompetenceRepository competenceRepository;
    @Autowired
    private PosteRepository posteRepository;
    @Autowired
    private MotifAbsenceRepository motifAbsenceRepository;
    @Autowired
    private Utils utils;
    @Autowired
    private EvaluationRepository evaluationRepository;
    @Autowired
    private DepartementRepository departementRepository;
    @Autowired
    private AbsenceRepository absenceRepository;
    @Autowired
    private EmployeDepartementRepository employeDepartementRepository;


    @RequestMapping("/accueil")
    public String accueil(Model model){
        int retraites =0;//      = employeRepository.countEmployeByDate_naissance();
        int fin_contrat =0;//    = contratRepository.countEmpFinContrat();
        int effectifActuel  = employeRepository.countEmployeById();
        int taux_turn_over  = effectifAnneeRepository.tauxTurnOver();
        int age=0;
        int fin=0;
        List<Employe> employes = employeRepository.findAll();
        for(Employe e:employes){
            age = utils.calculateAge(e.getDate_naissance(),LocalDate.now());
            if(age>=57){
                retraites++;
            }
        }
        for(Employe e:employes){
            for(Contrat c:e.getContrats()){
                fin = utils.calculateAge(c.getDate_debut(),c.getDate_fin());
                if(fin<=2 && c.getDate_debut()!=null){
                    fin_contrat++;
                }
            }
        }

        model.addAttribute("taux_turn_over", taux_turn_over);
        model.addAttribute("fin_contrat", fin_contrat);
        model.addAttribute("retraites", retraites);
        model.addAttribute("effectifActuel", effectifActuel);
        return "layouts/dashboard";
    }

    @GetMapping("/retraites")
    public String retraitesPage(Model model) {
        List<Employe> employes = employeRepository.findAll();
        List<Employe> employeArrayList = new ArrayList<>();
        int age=0;
        for(Employe e:employes){
            age = utils.calculateAge(e.getDate_naissance(),LocalDate.now());
            if(age>=57){
                employeArrayList.add(e);
            }
        }
        model.addAttribute("employes", employeArrayList);
        return "employes/retraites";
    }

    @GetMapping("/fincontrat")
    public String fincontratPage(Model model) {
        List<Employe> employes = employeRepository.findAll();
        List<Employe> employeList = new ArrayList<>();
        int fincontrat=0;
        Contrat contrat = new Contrat();
        for(Employe e :employes){
            for(Contrat c : e.getContrats()){
                fincontrat = utils.calculateAge(c.getDate_debut(),c.getDate_fin());
                if(fincontrat<=2 && c.getDate_debut()!=null){
                    employeList.add(e);
                    contrat=c;
                }
            }
        }
        model.addAttribute("employes", employeList);
        model.addAttribute("contrat", contrat);
        return "employes/fincontrat";
    }

    @GetMapping("/matrice")
    public String matricePage(Model model) {
        List<Employe> employes = employeRepository.employesEvalues();
        List<Evaluation> evaluations = new ArrayList<>();
        List<Evaluation> evaluationList = new ArrayList<>();
        for(Employe e:employes){
            evaluations = evaluationRepository.getEvaluationsMatrice(e.getId());
            evaluationList.addAll(evaluations);
        }
        List<Competence> competences = competenceRepository.getCompetenceMatrice();

        model.addAttribute("evaluations", evaluationList);
        model.addAttribute("competences", competences);
        model.addAttribute("employes", employes);
        return "employes/matricepolyvalence";
    }

    @RequestMapping("/pieChart")
    public ResponseEntity<?> getPieChartData(){
        int nbM=0,nbF=0;
        List<Employe> employes = employeRepository.findAll();
        List<Integer> list =new ArrayList<>();
        for(Employe employe:employes){
            if(employe.getSexe().equalsIgnoreCase("M")){
                nbM++;
            }else{
                nbF++;
            }
        }
        list.add(nbM);list.add(nbF);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping("/containerpa")
    public ResponseEntity<?> getPyramideDesAges(){
//        String[] tableau = {"20-24","25-29","30-34","35-39","40-44","45-49","50-54","55-59","60-64"};
        List<String> tableau = Stream.of("20-24","25-29","30-34","35-39","40-44","45-49","50-54","55-59","60-64").collect(Collectors.toList());
        List<Employe> employes = employeRepository.findAll();
        String[] split={};
        List<List> listeofliste = new ArrayList<>();
        List<String> liste  = new ArrayList<>();
        int un=0;
        int deux=0;
        for (String i : tableau) {
            split = i.split("-");
            un = Integer.parseInt(split[0]);
            deux = Integer.parseInt(split[1]);
            int incFemme=0;
            int incHomme=0;
            int age=0;
            for (Employe employe : employes) {
                age   = utils.calculateAge(employe.getDate_naissance(), LocalDate.now());
                if(age>=un && age<=deux){
                    if(employe.getSexe().equalsIgnoreCase("M")){
                        incHomme++;
                    }else{
                        incFemme++;
                    }
                }
            }
            liste.add(incHomme+"+"+incFemme);
        }
        listeofliste.add(liste);listeofliste.add(tableau);
        return new ResponseEntity<>(listeofliste, HttpStatus.OK);
    }


    @RequestMapping("/pyramideanciennetes")
    public ResponseEntity<?> getPyramideDesAncienntes(){
        List<String> tableau = Stream.of("0-2","3-5","6-8","9-11","12-14","15-17","18-20","21-23","24-26").collect(Collectors.toList());
        List<Employe> employes = employeRepository.findAll();
        String[] split={};
        List<List> listeofliste = new ArrayList<>();
        List<String> liste  = new ArrayList<>();
        int un=0;
        int deux=0;
        for (String i : tableau) {
            split = i.split("-");
            un = Integer.parseInt(split[0]);
            deux = Integer.parseInt(split[1]);
            int incFemme=0;
            int incHomme=0;
            int age=0;
            for (Employe employe : employes) {
                age   = utils.calculateAge(employe.getDate_recrutement(), LocalDate.now());
                if(age>=un && age<=deux){
                    if(employe.getSexe().equalsIgnoreCase("M")){
                        incHomme++;
                    }else{
                        incFemme++;
                    }
                }
            }
            liste.add(incHomme+"+"+incFemme);
        }
        listeofliste.add(liste);listeofliste.add(tableau);
        return new ResponseEntity<>(listeofliste, HttpStatus.OK);
    }


    @RequestMapping("/repartitionpartypedecontrat")
    public ResponseEntity<?> repartitionParTypedeContrat(){
        List<Employe> employes = employeRepository.findAll();
        int cdi=0,cdd=0,ctt=0,stage=0,autre=0;
        List<Integer> nombres = new ArrayList<>();
        for(Employe emp:employes){
            for(Contrat c: emp.getContrats()){
                if(c.getType_contrat().getLibelle_type_contrat().equalsIgnoreCase("CDI")){
                    cdi++;
                }
                if(c.getType_contrat().getLibelle_type_contrat().equalsIgnoreCase("CDD")){
                    cdd++;
                }
                if(c.getType_contrat().getLibelle_type_contrat().equalsIgnoreCase("CTT")){
                    ctt++;
                }
                if(c.getType_contrat().getLibelle_type_contrat().equalsIgnoreCase("Stage")){
                    stage++;
                }
                if(c.getType_contrat().getLibelle_type_contrat().equalsIgnoreCase("Autre")){
                    autre++;
                }
            }
        }
        nombres.add(cdi);nombres.add(cdd);nombres.add(ctt);nombres.add(stage);nombres.add(autre);
        return new ResponseEntity<>(nombres, HttpStatus.OK);
    }

    @RequestMapping("/evolutiondeseffectifsentreessorties")
    public ResponseEntity<?> evolutiondeseffectifsentreessorties(){
        List<EffectifAnnee> effectifAnnees = effectifAnneeRepository.findAll();
        List<String> code_annees = new ArrayList<>();
        List<Integer> entrees = new ArrayList<>();
        List<Integer> sorties = new ArrayList<>();
        List<List> listofliste = new ArrayList<>();
        for(EffectifAnnee eff:effectifAnnees){
            code_annees.add(eff.getAnnee().getCode_annee()+"");
            entrees.add(eff.getEffectif_entrant());
            sorties.add(eff.getEffectif_sortant());
        }
        listofliste.add(code_annees);listofliste.add(entrees);listofliste.add(sorties);
        return new ResponseEntity<>(listofliste, HttpStatus.OK);
    }

    @RequestMapping("/evolutiondeseffectifsparsexe")
    public ResponseEntity<?> evolutiondeseffectifsparsexe(){
        List<EffectifAnnee> effectifAnnees = effectifAnneeRepository.findAll();
        List<Integer> hommes = new ArrayList<>();
        List<Integer> femmes = new ArrayList<>();
        List<List> listofliste = new ArrayList<>();
        for(EffectifAnnee eff:effectifAnnees){
            hommes.add(eff.getEffectif_h_janvier());
            femmes.add(eff.getEffectif_f_janvier());
        }
       listofliste.add(hommes);listofliste.add(femmes);
        return new ResponseEntity<>(listofliste, HttpStatus.OK);
    }

    @RequestMapping("/repartitionpardepartement")
    public ResponseEntity<?> repartitionpardepartement(){
        List<Departement> departements = departementRepository.findAll();
        List<String[]> d = new ArrayList<>();
        for(Departement departement:departements){
            String[] tab = new String[2];
            int nbre=0;
            for(EmployeDepartement empt:departement.getEmploye_departements()){
                nbre++;
                tab[0]=departement.getCode_departement();
                tab[1]=nbre+"";
            }
            d.add(tab);
        }
        return new ResponseEntity<>(d, HttpStatus.OK);
    }

    @RequestMapping("/repartitionpartypedesortie")
    public ResponseEntity<?> repartitionpartypedesortie(){
        List<MotifAbsence> motifAbsences = motifAbsenceRepository.motifAbsences();
        List<String[]> d = new ArrayList<>();
        for(MotifAbsence mabs:motifAbsences){
            String[] tab = new String[2];
            int nbre=0;
            for(Absence abs:mabs.getAbsences()){
                if(abs.getMotif_absence().getLibelle_motif_absence()!=null){
                    nbre++;
                    tab[0]=mabs.getLibelle_motif_absence();
                    tab[1]=nbre+"";
                }
            }
            d.add(tab);
        }
        return new ResponseEntity<>(d, HttpStatus.OK);
    }

}
