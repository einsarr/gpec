package com.gpec.dao;

import com.gpec.model.Corps;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CorpsRepository extends JpaRepository<Corps, Integer> {
}
