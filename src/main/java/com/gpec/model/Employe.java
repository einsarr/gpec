package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
public class Employe implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique=true)
    private String matricule;
    @Column(unique=true)
    private String ien;
    private String prenom;
    private String nom;
    private String sexe;
    //@Temporal(TemporalType.DATE)
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    //private Date date_naissance;
    @Column(columnDefinition = "DATE")
    private LocalDate date_naissance;
    private String lieu_naissance;
    @Column(unique=true)
    private String cni;
    @Column(unique=true)
    private String telephone_prof;
    private String telephone_pers;
    @Column(unique=true)
    private String email_prof;
    private String email_pers;
    private String adresse;
    @Column(columnDefinition = "DATE")
    private LocalDate date_recrutement;
    private int etat;

    @JsonIgnore
    @OneToOne(mappedBy = "employe")
    private Utilisateur utilisateur;

    @JsonIgnore
    @OneToMany(mappedBy = "employe")
    private List<Evaluation> evaluations;

    @JsonIgnore
    @OneToMany(mappedBy = "employe")
    private List<EntretienAnnuel> entretienAnnuels;

    @JsonIgnore
    @OneToMany(mappedBy = "employe")
    private List<Contrat> contrats;
    @JsonIgnore
    @OneToMany(mappedBy = "employe")
    private List<EmployeDepartement> employe_departements;

    @JsonIgnore
    @OneToMany(mappedBy = "employe")
    private List<EmployeAutreFormation> employe_autreFormations;

    @JsonIgnore
    @OneToMany(mappedBy = "employe")
    private List<EmployePoste> poste_employes;

    @JsonIgnore
    @OneToMany(mappedBy = "employe")
    private List<EmployeDiplomeAcademique> employe_diplomeAcademiques;

    @JsonIgnore
    @OneToMany(mappedBy = "employe")
    private List<EmployeDiplomeProfessionnel> employe_diplomeProfs;

    @ManyToOne
    @JoinColumn(name = "corps_id")
    private Corps corps;

    @ManyToOne
    @JoinColumn(name = "grade_id")
    private Grade grade;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getIen() {
        return ien;
    }

    public void setIen(String ien) {
        this.ien = ien;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public List<EntretienAnnuel> getEntretienAnnuels() {
        return entretienAnnuels;
    }

    public void setEntretienAnnuels(List<EntretienAnnuel> entretienAnnuels) {
        this.entretienAnnuels = entretienAnnuels;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public LocalDate getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(LocalDate date_naissance) {
        this.date_naissance = date_naissance;
    }

    public String getLieu_naissance() {
        return lieu_naissance;
    }

    public void setLieu_naissance(String lieu_naissance) {
        this.lieu_naissance = lieu_naissance;
    }

    public String getCni() {
        return cni;
    }

    public void setCni(String cni) {
        this.cni = cni;
    }

    public String getTelephone_prof() {
        return telephone_prof;
    }

    public void setTelephone_prof(String telephone_prof) {
        this.telephone_prof = telephone_prof;
    }

    public String getTelephone_pers() {
        return telephone_pers;
    }

    public void setTelephone_pers(String telephone_pers) {
        this.telephone_pers = telephone_pers;
    }

    public String getEmail_prof() {
        return email_prof;
    }

    public void setEmail_prof(String email_prof) {
        this.email_prof = email_prof;
    }

    public String getEmail_pers() {
        return email_pers;
    }

    public void setEmail_pers(String email_pers) {
        this.email_pers = email_pers;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public LocalDate getDate_recrutement() {
        return date_recrutement;
    }

    public void setDate_recrutement(LocalDate date_recrutement) {
        this.date_recrutement = date_recrutement;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public List<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }

    public List<EmployeDepartement> getEmploye_departements() {
        return employe_departements;
    }

    public void setEmploye_departements(List<EmployeDepartement> employe_departements) {
        this.employe_departements = employe_departements;
    }

    public void setContrats(List<Contrat> contrats) {
        this.contrats = contrats;
    }

    public List<EmployeAutreFormation> getEmploye_autreFormations() {
        return employe_autreFormations;
    }

    public void setEmploye_autreFormations(List<EmployeAutreFormation> employe_autreFormations) {
        this.employe_autreFormations = employe_autreFormations;
    }

    public List<EmployePoste> getPoste_employes() {
        return poste_employes;
    }

    public void setPoste_employes(List<EmployePoste> poste_employes) {
        this.poste_employes = poste_employes;
    }

    public List<EmployeDiplomeAcademique> getEmploye_diplomeAcademiques() {
        return employe_diplomeAcademiques;
    }

    public void setEmploye_diplomeAcademiques(List<EmployeDiplomeAcademique> employe_diplomeAcademiques) {
        this.employe_diplomeAcademiques = employe_diplomeAcademiques;
    }

    public List<EmployeDiplomeProfessionnel> getEmploye_diplomeProfs() {
        return employe_diplomeProfs;
    }

    public void setEmploye_diplomeProfs(List<EmployeDiplomeProfessionnel> employe_diplomeProfs) {
        this.employe_diplomeProfs = employe_diplomeProfs;
    }

    public List<Contrat> getContrats() {
        return contrats;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public Corps getCorps() {
        return corps;
    }

    public void setCorps(Corps corps) {
        this.corps = corps;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }
}
