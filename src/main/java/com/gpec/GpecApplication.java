package com.gpec;

import com.gpec.dao.CompetenceRepository;
import com.gpec.service.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;

@SpringBootApplication
@EnableScheduling
public class GpecApplication implements CommandLineRunner {
    @Autowired
    BCryptPasswordEncoder encoder;

    @Autowired
    CompetenceRepository competenceRepository;
    @Autowired
    Utils utils;
    public static void main(String[] args) {
        SpringApplication.run(GpecApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        LocalDate localDate1 = LocalDate.of(2000, 5, 20);
        //System.out.println(encoder.encode("passer"));
        //System.out.println(utils.calculateAge(localDate1,LocalDate.now()));
        System.out.println("ok");
        //System.out.println(utils.getRandomNumberString());
       // System.out.println(competenceRepository.getLastCompetence());
//        System.out.println(utils.intToString(8,4));
    }
}
