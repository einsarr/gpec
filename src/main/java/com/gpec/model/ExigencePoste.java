package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "UniqueNumberAndStatus", columnNames = { "formation_initiale", "experience_professionnelle", "condition_exercice" }) })
public class ExigencePoste implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String formation_initiale;
    private String experience_professionnelle;
    private String condition_exercice;

    @JsonIgnore
    @OneToMany(mappedBy = "exigencePoste")
    private List<Poste> postes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFormation_initiale() {
        return formation_initiale;
    }

    public void setFormation_initiale(String formation_initiale) {
        this.formation_initiale = formation_initiale;
    }

    public String getExperience_professionnelle() {
        return experience_professionnelle;
    }

    public void setExperience_professionnelle(String experience_professionnelle) {
        this.experience_professionnelle = experience_professionnelle;
    }

    public String getCondition_exercice() {
        return condition_exercice;
    }

    public void setCondition_exercice(String condition_exercice) {
        this.condition_exercice = condition_exercice;
    }

    public List<Poste> getPostes() {
        return postes;
    }

    public void setPostes(List<Poste> postes) {
        this.postes = postes;
    }
}
