package com.gpec.controller;

import com.gpec.dao.RoleRepository;
import com.gpec.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
public class RoleController implements Serializable {

    @Autowired
    private RoleRepository roleRepository;


    @GetMapping("/roles")
    public String userPage(Model model) {
        Role role = new Role();
        model.addAttribute("role", role);
        List<Role> roles = roleRepository.findAll();
        model.addAttribute("roles", roles);
        return "securite/roles";
    }
    @PostMapping("/addRole")
    public String addRole(@ModelAttribute("role") Role role) {
        try {
            roleRepository.save(role);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/roles";
    }

    @GetMapping("/role/{id}")
    public @ResponseBody
    Role getRole(@PathVariable(name = "id") int roleId){
        return roleRepository.findById(roleId).get();
    }


    @PostMapping("/removeRole")
    public String removeRole(int userId) {
        Role r = roleRepository.findById(userId).get();
        roleRepository.delete(r);
        return "redirect:/roles";
    }
}
