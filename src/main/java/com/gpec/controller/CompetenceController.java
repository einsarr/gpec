package com.gpec.controller;

import com.gpec.dao.*;
import com.gpec.model.*;
import com.gpec.service.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@PreAuthorize("hasAuthority('ROLE_DIRECTEUR')")
public class CompetenceController implements Serializable {
    @Autowired
    private CompetenceRepository competenceRepository;
    @Autowired
    private PosteRepository posteRepository;
    @Autowired
    private EmployeRepository employeRepository;
    @Autowired
    private Utils utils;
    @Autowired
    private CategorieCompetenceRepository categorieCompetenceRepository;

    @Autowired
    private NiveauMaitriseCptRequisRepository niveauMaitriseCptRequisRepository;

    @GetMapping("/competences")
    public String CompetencesPage(Model model) {
        Competence competence = new Competence();
        model.addAttribute("competence", competence);
        List<CategorieCompetence> categorieCompetences  = categorieCompetenceRepository.findAll();
        List<NiveauMaitriseCptRequis> niveauMaitriseCptRequis  = niveauMaitriseCptRequisRepository.findAll();
        List<Competence> competences                    = competenceRepository.findAll();
        model.addAttribute("categories", categorieCompetences);
        model.addAttribute("niveauMatrises", niveauMaitriseCptRequis);
        model.addAttribute("competences", competences);
        return "parametrage/competences";
    }
    @PostMapping("/addCompetence")
    public String addCompetence(@ModelAttribute("competence") Competence competence) {
        if(competence.getId()==null){
            boolean cs   =   competence.getCategorie().getLibelle_categorie().equalsIgnoreCase("Savoir");
            boolean csf  =   competence.getCategorie().getLibelle_categorie().equalsIgnoreCase("Savoir-Faire");
            boolean cse  =   competence.getCategorie().getLibelle_categorie().equalsIgnoreCase("Savoir-Etre");
            long maxId   =   competenceRepository.getLastCompetence()+1;
            if(cs) competence.setCode_competence("CS"+utils.codification((int)maxId,4));
            if(csf) competence.setCode_competence("CSF"+utils.codification((int)maxId,4));
            if(cse) competence.setCode_competence("CSE"+utils.codification((int)maxId,4));
        }
        try {
            competenceRepository.save(competence);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/competences";
    }

    @GetMapping("/competence/{id}")
    public @ResponseBody
    Competence getCompetence(@PathVariable(name = "id")long id){
        return competenceRepository.findById(id).get();
    }

    @PostMapping("/removeCompetence")
    public String removeCompetence(long competenceId) {
        Competence g = competenceRepository.findById(competenceId).get();
        competenceRepository.delete(g);
        return "redirect:/competences";
    }

    @GetMapping("/repartitionCompetences")
    public String repartitionCompetences(Model model,@RequestParam(name="ageretraite",defaultValue = "60")int ageretraite,@RequestParam(name="anneeprojection",defaultValue = "5")Integer anneeprojection) {
        List<Poste> postes = new ArrayList<>();
        List<Competence> competences = new ArrayList<>();
        List<Employe> employes = employeRepository.findAll();
        List<String> couleurs = Stream.of("success","primary","warning","danger","inverse","pink").collect(Collectors.toList());
        for(Employe emp:employes){
            if(utils.calculateAge(emp.getDate_naissance(), LocalDate.now())+anneeprojection>=ageretraite){
                Poste poste = posteRepository.findPosteByEmploye(emp.getId());
                if(poste!=null){
                    postes.add(poste);
                    List<Competence> cpt = competenceRepository.getCompetenceByPoste(poste.getId());
                    competences.addAll(cpt);
                }
            }
        }
        model.addAttribute("competences",competences);
        model.addAttribute("postes",postes);
        model.addAttribute("couleurs",couleurs);
        model.addAttribute("ageretaite",ageretraite);
        model.addAttribute("anneeprojection",anneeprojection);
        return "employes/repartitiondescompetences";
    }

    @GetMapping("/repartition_competences")
    public ResponseEntity<?> repartitionCompetence(Model model) {
        List<Competence> competences = competenceRepository.findAll();
        List<String> listeCodescpt = new ArrayList<>();
        List<Integer> valeurPostes = new ArrayList<>();
        List<List> listeofliste = new ArrayList<>();
        for(Competence c :competences){
            for(PosteCompetence pct: c.getPoste_competences()){
                valeurPostes.add((int) Stream.of(pct.getCompetence()).count());
            }
            listeCodescpt.add(c.getCode_competence());
        }
        listeofliste.add(listeCodescpt);listeofliste.add(valeurPostes);
        return new ResponseEntity<>(listeofliste, HttpStatus.OK);
    }


}
