package com.gpec.controller;

import com.gpec.dao.*;
import com.gpec.model.*;
import groovy.util.Eval;
import org.apache.catalina.connector.Response;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Controller
public class EvaluationController implements Serializable {

    @Autowired
    private EmployeRepository employeRepository;
    @Autowired
    private PosteRepository posteRepository;
    @Autowired
    private PotentielRepository potentielRepository;
    @Autowired
    private QualificationRepository qualificationRepository;
    @Autowired
    private EfficaciteRepository efficaciteRepository;
    @Autowired
    private CompetenceRepository competenceRepository;
    @Autowired
    private EvaluationRepository evaluationRepository;

    private AnneeRepository anneeRepository;
    @Autowired
    private NiveauMaitriseCptRequisRepository niveauRepo;
    @RequestMapping("/evaluer")
    public String evaluerEmploye(@RequestParam(name = "id") Long employeId,Model model) {
        try {
            Employe employe = employeRepository.findById(employeId).get();
            Evaluation evaluation = new Evaluation();
            Poste poste = posteRepository.findPosteByEmploye(employeId);
            List<NiveauMaitriseCptRequis> maitrises = niveauRepo.findAll();
            List<Competence> competences = competenceRepository.getCompetenceByPoste(poste.getId());
            model.addAttribute("competences", competences);
            model.addAttribute("poste", poste);
            model.addAttribute("maitrises", maitrises);
            model.addAttribute("evaluation", evaluation);
            model.addAttribute("employe", employe);

        }catch (Exception e){
            System.out.println(e.fillInStackTrace());
        }
        return "employes/evaluation";
    }

    @RequestMapping("/formations")
    public String besoinsEnFormations(Model model) {
        List<Employe> employes = employeRepository.employesEvalues();
        List<Employe> employeList = new ArrayList<>();
        List<Poste> postes = new ArrayList<>();
        int compteur=0;
        List<Competence> competenceList = new ArrayList<>();
        for(Employe employe:employes){
            Poste poste = posteRepository.findPosteByEmploye(employe.getId());
            if(poste!=null){
                List<Competence> competences = competenceRepository.getCompetenceByPoste(poste.getId());
                List<Evaluation> evaluations = evaluationRepository.evaluationsByEmploye(employe.getId());
                for(Evaluation evaluation:evaluations){
                    for(Competence competence:competences){
                        if(competence.getNiveauMaitriseCptRequis().getValeur_niv()>evaluation.getCompetence().getNiveauMaitriseCptRequis().getValeur_niv()){
//                            competenceList.add(competence);
//                            postes.add(poste);
                            compteur++;

                        }
                    }
                }
            }
            if(compteur>0) employeList.add(employe);
        }
        model.addAttribute("employes", employeList);
        model.addAttribute("postes", postes);
//        model.addAttribute("competences", competenceList);
        return "employes/formations";
    }

    @RequestMapping("/entretiensannuel")
    public String entretiensannuel(@RequestParam(name = "id") Long employeId,Model model) {
        try{
            Employe employe = employeRepository.findById(employeId).get();
            Poste poste = posteRepository.findPosteByEmploye(employeId);
            List<Potentiel> potentiels = potentielRepository.findAll();
            List<Qualification> qualifications = qualificationRepository.findAll();
            List<Efficacite> efficacites = efficaciteRepository.findAll();
            model.addAttribute("poste", poste);
            model.addAttribute("employe", employe);
            model.addAttribute("potentiels", potentiels);
            model.addAttribute("qualifications", qualifications);
            model.addAttribute("efficacites", efficacites);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "employes/entretiensannuel";
    }

}
