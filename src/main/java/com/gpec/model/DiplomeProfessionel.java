package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class DiplomeProfessionel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String code_dipl_prof;
    @Column(unique=true)
    private String libelle_dipl_prof;
    private String institut_ecole_prof;
    private int etat_dipl_prof;

    @JsonIgnore
    @OneToMany(mappedBy = "diplome_professionnel")
    private List<EmployeDiplomeProfessionnel> employe_diplomeProfs;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode_dipl_prof() {
        return code_dipl_prof;
    }

    public void setCode_dipl_prof(String code_dipl_prof) {
        this.code_dipl_prof = code_dipl_prof;
    }

    public String getLibelle_dipl_prof() {
        return libelle_dipl_prof;
    }

    public void setLibelle_dipl_prof(String libelle_dipl_prof) {
        this.libelle_dipl_prof = libelle_dipl_prof;
    }

    public String getInstitut_ecole_prof() {
        return institut_ecole_prof;
    }

    public void setInstitut_ecole_prof(String institut_ecole_prof) {
        this.institut_ecole_prof = institut_ecole_prof;
    }

    public int getEtat_dipl_prof() {
        return etat_dipl_prof;
    }

    public void setEtat_dipl_prof(int etat_dipl_prof) {
        this.etat_dipl_prof = etat_dipl_prof;
    }

    public List<EmployeDiplomeProfessionnel> getEmploye_diplomeProfs() {
        return employe_diplomeProfs;
    }

    public void setEmploye_diplomeProfs(List<EmployeDiplomeProfessionnel> employe_diplomeProfs) {
        this.employe_diplomeProfs = employe_diplomeProfs;
    }
}
