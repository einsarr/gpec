package com.gpec.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class RelationFonctionnelle implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String libelle_relation_fonct;
    @Column(unique=true)
    private String type_relation_fonct;
    private int etat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle_relation_fonct() {
        return libelle_relation_fonct;
    }

    public void setLibelle_relation_fonct(String libelle_relation_fonct) {
        this.libelle_relation_fonct = libelle_relation_fonct;
    }

    public String getType_relation_fonct() {
        return type_relation_fonct;
    }

    public void setType_relation_fonct(String type_relation_fonct) {
        this.type_relation_fonct = type_relation_fonct;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
}
