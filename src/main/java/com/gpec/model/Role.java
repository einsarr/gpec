package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String libelle_role;
    private int etat_role;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles")
    @JsonIgnoreProperties("roles")
    private List<Utilisateur> utilisateurs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelle_role() {
        return libelle_role;
    }

    public void setLibelle_role(String libelle_role) {
        this.libelle_role = libelle_role;
    }

    public int getEtat_role() {
        return etat_role;
    }

    public List<Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }

    public void setUtilisateurs(List<Utilisateur> utilisateurs) {
        this.utilisateurs = utilisateurs;
    }
}
