package com.gpec.controller;

import com.gpec.dao.MotifAbsenceRepository;
import com.gpec.model.MotifAbsence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class MotifAbsenceController implements Serializable {
    @Autowired
    private MotifAbsenceRepository motif_absenceRepository;

    @GetMapping("/motif_absences")
    public String MotifAbsencesPage(Model model) {
        MotifAbsence motif_absence = new MotifAbsence();
        model.addAttribute("motif_absence", motif_absence);
        List<MotifAbsence> motif_absences = motif_absenceRepository.findAll();
        model.addAttribute("motif_absences", motif_absences);
        return "employes/motif_absences";
    }
    @PostMapping("/addMotifAbsence")
    public String addMotifAbsence(@ModelAttribute("motif_absence") MotifAbsence motif_absence) {
        try {
            motif_absenceRepository.save(motif_absence);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/motif_absences";
    }

    @GetMapping("/motif_absence/{id}")
    public @ResponseBody
    MotifAbsence getMotifAbsence(@PathVariable(name = "id") int motif_absenceId){
        return motif_absenceRepository.findById(motif_absenceId).get();
    }


    @PostMapping("/removeMotifAbsence")
    public String removeMotifAbsence(int motif_absenceId) {
        MotifAbsence g = motif_absenceRepository.findById(motif_absenceId).get();
        motif_absenceRepository.delete(g);
        return "redirect:/motif_absences";
    }
}
