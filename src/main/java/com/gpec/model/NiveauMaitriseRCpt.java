package com.gpec.model;

public enum NiveauMaitriseRCpt {
    AUCUNE_MAITRISE,MAITRISE_APRES_FORMATION,BONNE_MAITRISE,EXPERT
}
