package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Departement implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String code_departement;
    @Column(unique=true)
    private String description_departement;
    private int etat_departement;

    @JsonIgnore
    @OneToOne(mappedBy = "departement")
    private Utilisateur utilisateur;

    @JsonIgnore
    @OneToMany(mappedBy = "departement")
    private List<EmployeDepartement> employe_departements;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode_departement() {
        return code_departement;
    }

    public void setCode_departement(String code_departement) {
        this.code_departement = code_departement;
    }

    public String getDescription_departement() {
        return description_departement;
    }

    public void setDescription_departement(String description_departement) {
        this.description_departement = description_departement;
    }

    public int getEtat_departement() {
        return etat_departement;
    }

    public void setEtat_departement(int etat_departement) {
        this.etat_departement = etat_departement;
    }

    public List<EmployeDepartement> getEmploye_departements() {
        return employe_departements;
    }

    public void setEmploye_departements(List<EmployeDepartement> employe_departements) {
        this.employe_departements = employe_departements;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }
}
