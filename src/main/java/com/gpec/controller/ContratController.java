package com.gpec.controller;

import com.gpec.dao.ContratRepository;
import com.gpec.dao.EmployeRepository;
import com.gpec.dao.TypeContratRepository;
import com.gpec.model.Contrat;
import com.gpec.model.Employe;
import com.gpec.model.TypeContrat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class ContratController implements Serializable {
    @Autowired
    private ContratRepository contratRepository;

    @Autowired
    private EmployeRepository employeRepository;

    @Autowired
    private TypeContratRepository typeContratRepository;

    @GetMapping("/contrats")
    public String ContratsPage(Model model) {
        Contrat contrat         = new Contrat();
        model.addAttribute("contrat", contrat);
        List<TypeContrat> typeContrats  = typeContratRepository.findAll();
        List<Employe> employes  = employeRepository.findAll();
        List<Contrat> contrats  = contratRepository.findAll();
        model.addAttribute("typeContrats", typeContrats);
        model.addAttribute("employes", employes);
        model.addAttribute("contrats", contrats);
        return "employes/contrats";
    }
    @PostMapping("/addContrat")
    public String addContrat(@ModelAttribute("contrat") Contrat contrat) {
        try {
            contratRepository.save(contrat);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/contrats";
    }

    @GetMapping("/contrat/{id}")
    public @ResponseBody
    Contrat getContrat(@PathVariable(name = "id") int contratId){
        return contratRepository.findById(contratId).get();
    }


    @PostMapping("/removeContrat")
    public String removeContrat(int contratId) {
        Contrat g = contratRepository.findById(contratId).get();
        contratRepository.delete(g);
        return "redirect:/contrats";
    }
}
