package com.gpec.dao;

import com.gpec.model.DiplomeAcademique;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiplomeAcademiqueRepository extends JpaRepository<DiplomeAcademique, Integer> {
}
