package com.gpec.dao;

import com.gpec.model.UniversiteEcoleInstitut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UniversiteEcoleInstitutRepository extends JpaRepository<UniversiteEcoleInstitut, Integer> {
}
