package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Mission implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String libelle_mission;
    private int etat_mission;

    @JsonIgnore
    @OneToMany(mappedBy = "mission")
    private List<PosteMission> poste_missions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle_mission() {
        return libelle_mission;
    }

    public void setLibelle_mission(String libelle_mission) {
        this.libelle_mission = libelle_mission;
    }

    public int getEtat_mission() {
        return etat_mission;
    }

    public void setEtat_mission(int etat_mission) {
        this.etat_mission = etat_mission;
    }

    public List<PosteMission> getPoste_missions() {
        return poste_missions;
    }

    public void setPoste_missions(List<PosteMission> poste_missions) {
        this.poste_missions = poste_missions;
    }
}
