package com.gpec.controller;

import com.gpec.dao.DiplomeProfessionnelRepository;
import com.gpec.model.DiplomeProfessionel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class DiplomeProfessionelController implements Serializable {
    @Autowired
    private DiplomeProfessionnelRepository diplomeProfessionnelRepository;

    @GetMapping("/diplome_professionnels")
    public String diplomeprofessionnelPage(Model model) {
        DiplomeProfessionel diplome_professionnel = new DiplomeProfessionel();
        model.addAttribute("diplome_professionnel", diplome_professionnel);
        List<DiplomeProfessionel> diplome_professionnels = diplomeProfessionnelRepository.findAll();
        model.addAttribute("diplome_professionnels", diplome_professionnels);
        return "employes/diplome_professionnels";
    }
    @PostMapping("/addDiplomeProfessionnel")
    public String addcorps(@ModelAttribute("diplome_professionnel") DiplomeProfessionel diplome_professionnel) {
        try {
            diplomeProfessionnelRepository.save(diplome_professionnel);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/diplome_professionnels";
    }

    @GetMapping("/diplome_professionnel/{id}")
    public @ResponseBody
    DiplomeProfessionel getDiplomeProfessionnel(@PathVariable(name = "id") int diplome_professionnelId){
        return diplomeProfessionnelRepository.findById(diplome_professionnelId).get();
    }


    @PostMapping("/removeDiplomeProfessionnel")
    public String removeDiplomeProfessionnel(int diplome_professionnelId) {
        DiplomeProfessionel g = diplomeProfessionnelRepository.findById(diplome_professionnelId).get();
        diplomeProfessionnelRepository.delete(g);
        return "redirect:/diplome_professionnels";
    }
}
