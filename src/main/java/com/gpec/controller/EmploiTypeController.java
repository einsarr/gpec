package com.gpec.controller;

import com.gpec.dao.EmploiTypeRepository;
import com.gpec.model.EmploiType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
public class EmploiTypeController implements Serializable {
    @Autowired
    private EmploiTypeRepository emploiTypeRepository;

    @GetMapping("/emploi_types")
    public String EmploiTypesPage(Model model) {
        EmploiType emploi_type = new EmploiType();
        model.addAttribute("emploi_type", emploi_type);
        List<EmploiType> emploi_types = emploiTypeRepository.findAll();
        model.addAttribute("emploi_types", emploi_types);
        return "parametrage/emploi_types";
    }
    @PostMapping("/addEmploiType")
    public String addEmploiType(@ModelAttribute("emploi_type") EmploiType emploi_type) {
        try {
            emploiTypeRepository.save(emploi_type);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/emploi_types";
    }

    @GetMapping("/emploi_type/{id}")
    public @ResponseBody
    EmploiType getGrade(@PathVariable(name = "id") int emploitId){
        return emploiTypeRepository.findById(emploitId).get();
    }


    @PostMapping("/removeEmploi_type")
    public String removeGrade(int emploitId) {
        EmploiType g = emploiTypeRepository.findById(emploitId).get();
        emploiTypeRepository.delete(g);
        return "redirect:/emploi_types";
    }
}
