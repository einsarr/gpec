package com.gpec.controller;
import com.gpec.dao.EmployeRepository;
import com.gpec.model.Employe;
import com.gpec.service.ReportService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.List;

@RestController
@CrossOrigin("*")
public class JasperReportController {
    @Autowired
    private EmployeRepository employeRepository;

    @Autowired
    private ReportService reportService;

    @GetMapping("getEmployes")
    public List<Employe> employes(){
        return employeRepository.findAll();
    }

    @GetMapping("/report/{format}")
    public String generateReport(@PathVariable String format) throws JRException, FileNotFoundException {
        return reportService.exportReport(format);
    }
}
