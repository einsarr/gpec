package com.gpec.dao;

import com.gpec.model.AutreFormation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AutreFormationRepository extends JpaRepository<AutreFormation, Integer> {
}
