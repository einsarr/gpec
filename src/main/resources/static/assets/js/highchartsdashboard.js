$(document).ready(function(){
    $.ajax({
        url:'pieChart',
        dataType:"json",
        success:function (result) {
            repartitionparsexe(result[0],result[1]);
        }
    });

    /**
    PYRAMIDE DES AGES
    **/
    $.ajax({
        url:'containerpa',
        dataType:"json",
        success:function(result){
            var valeurs = result[0];
            var categories =[];
            var valeursH=[];
            var valeursF=[];
            for (let i = 0; i < valeurs.length; i++) {
              const hommefemme=valeurs[i].split('+');
              valeursH.push(+hommefemme[0]);
              valeursF.push(-+hommefemme[1]);
            }
            categories=result[1];
            pyramidedesages(categories,valeursH,valeursF)
        }
    });

    /**
    PYRAMIDE DES ANCIENNTES
    **/
    $.ajax({
        url:'pyramideanciennetes',
        dataType:"json",
        success:function(result){
            var valeurs = result[0];
            var categories =[];
            var valeursH=[];
            var valeursF=[];
            for (let i = 0; i < valeurs.length; i++) {
              const hommefemme=valeurs[i].split('+');
              valeursH.push(+hommefemme[0]);
              valeursF.push(-+hommefemme[1]);
            }
            categories=result[1];
            repartitionparanciennete(categories,valeursH,valeursF)
        }
    });

    /**
        REPARTITION PAR TYPE DE CONTRATS
        **/
    $.ajax({
        url:'repartitionpartypedecontrat',
        dataType:"json",
        success:function(result){
            repartitionpartypedecontrat(result[0],result[1],result[2],result[3],result[4])
        }
    });

    /**
    EVOLUTION ENTREES SORTIES
    **/
    $.ajax({
        url:'evolutiondeseffectifsentreessorties',
        dataType:"json",
        success:function(result){
            evolutiondeseffectifsentreessorties(result[0],result[1],result[2])
        }
    });

    /**
    EVOLUTION ENTREES SORTIES
    **/
    $.ajax({
        url:'evolutiondeseffectifsparsexe',
        dataType:"json",
        success:function(result){
            evolutiondeseffectifsparsexe(result[0],result[1])
        }
    });

    /**
    REPARTITION PAR DEPARTEMENT
    **/
    $.ajax({
        url:'repartitionpardepartement',
        dataType:"json",
        success:function(result){
            const tab1 = [];
            for (let i = 0; i < result.length; i++) {
                const tab = [];
                tab[0]=result[i][0];
                tab[1]=parseInt(result[i][1]);
                tab1.push(tab);
            }
            repartitionpardepartement(tab1);
        }
    });

    /**
    REPARTITION PAR TYPE DE SORTIE
    **/
    $.ajax({
        url:'repartitionpartypedesortie',
        dataType:"json",
        success:function(result){
            const tab1 = [];
            for (let i = 0; i < result.length; i++) {
                const tab = [];
                tab[0]=result[i][0];
                tab[1]=parseInt(result[i][1]);
                tab1.push(tab);
            }
            repartitionpartypedesortie(tab1);
        }
    });

});

function pyramidedesages(categories,valeursH,valeursF){
    // Data gathered from http://populationpyramid.net/germany/2015/

// Age categories
    var categories = categories;

    Highcharts.chart('containerpa', {
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        accessibility: {
            point: {
                valueDescriptionFormat: '{index}. Age {xDescription}, {value}%.'
            }
        },
        xAxis: [{
            categories: categories,
            reversed: false,
            labels: {
                step: 1
            },
            accessibility: {
                description: 'Age (Homme)'
            }
        }, { // mirror axis on right side
            opposite: true,
            reversed: false,
            categories: categories,
            linkedTo: 0,
            labels: {
                step: 1
            },
            accessibility: {
                description: 'Age (Femme)'
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return Math.abs(this.value) + '%';
                }
            },
            accessibility: {
                description: 'Percentage population',
                rangeDescription: 'Range: 0 to 5%'
            }
        },

        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
                    'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 1) + '%';
            }
        },

        series: [{
            name: 'Femme',
            data: valeursF
        }, {
            name: 'Homme',
            data:valeursH
        }]
    });
}
function repartitionparsexe(m,f){
    Highcharts.chart('containerreps', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                },
                showInLegend: true
            }
        },
        series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Homme',
                    y: m,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Femme',
                    y: f
                }]
            }]
    });
}
function repartitionparanciennete(categories,valeursH,valeursF){
   // Data gathered from http://populationpyramid.net/germany/2015/

// Age categories
    var categories = categories;
    Highcharts.chart('containerrepa', {
chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        accessibility: {
            point: {
                valueDescriptionFormat: '{index}. Ancienneté {xDescription}, {value}%.'
            }
        },
        xAxis: [{
            categories: categories,
            reversed: false,
            labels: {
                step: 1
            },
            accessibility: {
                description: 'Ancienneté (Homme)'
            }
        }, { // mirror axis on right side
            opposite: true,
            reversed: false,
            categories: categories,
            linkedTo: 0,
            labels: {
                step: 1
            },
            accessibility: {
                description: 'Anciennté (Femme)'
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return Math.abs(this.value) + '%';
                }
            },
            accessibility: {
                description: 'Pourcentage employés population',
                rangeDescription: 'Range: 0 to 5%'
            }
        },

        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + ', ancienneté ' + this.point.category + '</b><br/>' +
                    'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 1) + '%';
            }
        },

        series: [{
            name: 'Femme',
            data: valeursF
        }, {
            name: 'Homme',
            data: valeursH
        }]
    });
}
function repartitionpartypedecontrat(cdi,cdd,ctt,stage,autre){
     Highcharts.chart('containerreptc', {
         chart: {
             type: 'column'
         },
         title: {
             text: ''
         },
         subtitle: {
             text:''
         },
         accessibility: {
             announceNewData: {
                 enabled: true
             }
         },
         xAxis: {
             type: 'category'
         },
         yAxis: {
             title: {
                 text: 'Repartition par type de contrat'
             }

         },
         legend: {
             enabled: false
         },
         plotOptions: {
             series: {
                 borderWidth: 0,
                 dataLabels: {
                     enabled: true,
                     format: '{point.y:.1f}%'
                 }
             }
         },

         tooltip: {
             headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
             pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
         },

         series: [
             {
                 name: "Type de contrat",
                 colorByPoint: true,
                 data: [
                     {
                         name: "CDI",
                         y: cdi,
                         drilldown: "CDI"
                     },
                     {
                         name: "CDD",
                         y: cdd,
                         drilldown: "CDD"
                     },
                     {
                         name: "CTT",
                         y: ctt,
                         drilldown: "CTT"
                     },
                     {
                         name: "Stage",
                         y: stage,
                         drilldown: "Stage"
                     },
                     {
                         name: "Autre",
                         y: autre,
                         drilldown: null
                     }
                 ]
             }
         ],
         drilldown: {
             series: [
                 {
                     name: "CDI",
                     id: "CDI",
                     data: [
                         [
                             "v65.0",
                             0.1
                         ],
                         [
                             "v64.0",
                             1.3
                         ],
                         [
                             "v63.0",
                             53.02
                         ],
                         [
                             "v62.0",
                             1.4
                         ],
                         [
                             "v61.0",
                             0.88
                         ],
                         [
                             "v60.0",
                             0.56
                         ],
                         [
                             "v59.0",
                             0.45
                         ],
                         [
                             "v58.0",
                             0.49
                         ],
                         [
                             "v57.0",
                             0.32
                         ],
                         [
                             "v56.0",
                             0.29
                         ],
                         [
                             "v55.0",
                             0.79
                         ],
                         [
                             "v54.0",
                             0.18
                         ],
                         [
                             "v51.0",
                             0.13
                         ],
                         [
                             "v49.0",
                             2.16
                         ],
                         [
                             "v48.0",
                             0.13
                         ],
                         [
                             "v47.0",
                             0.11
                         ],
                         [
                             "v43.0",
                             0.17
                         ],
                         [
                             "v29.0",
                             0.26
                         ]
                     ]
                 },
                 {
                     name: "CDI",
                     id: "CDI",
                     data: [
                         [
                             "v58.0",
                             1.02
                         ],
                         [
                             "v57.0",
                             7.36
                         ],
                         [
                             "v56.0",
                             0.35
                         ],
                         [
                             "v55.0",
                             0.11
                         ],
                         [
                             "v54.0",
                             0.1
                         ],
                         [
                             "v52.0",
                             0.95
                         ],
                         [
                             "v51.0",
                             0.15
                         ],
                         [
                             "v50.0",
                             0.1
                         ],
                         [
                             "v48.0",
                             0.31
                         ],
                         [
                             "v47.0",
                             0.12
                         ]
                     ]
                 },
                 {
                     name: "CTT",
                     id: "CTT",
                     data: [
                         [
                             "v11.0",
                             6.2
                         ],
                         [
                             "v10.0",
                             0.29
                         ],
                         [
                             "v9.0",
                             0.27
                         ],
                         [
                             "v8.0",
                             0.47
                         ]
                     ]
                 },
                 {
                     name: "Stage",
                     id: "Stage",
                     data: [
                         [
                             "v11.0",
                             3.39
                         ],
                         [
                             "v10.1",
                             0.96
                         ],
                         [
                             "v10.0",
                             0.36
                         ],
                         [
                             "v9.1",
                             0.54
                         ],
                         [
                             "v9.0",
                             0.13
                         ],
                         [
                             "v5.1",
                             0.2
                         ]
                     ]
                 },
                 {
                     name: "Autre",
                     id: "Autre",
                     data: [
                         [
                             "v50.0",
                             0.96
                         ],
                         [
                             "v49.0",
                             0.82
                         ],
                         [
                             "v12.1",
                             0.14
                         ]
                     ]
                 }
             ]
         }
     });
}
function evolutiondeseffectifsentreessorties(categ,entrees,sorties){
   Highcharts.chart('evolutiondeseffectifsentreessorties', {
       chart: {
           type: 'column'
       },
       title: {
           text: ''
       },
       subtitle: {
           text: ''
       },
       xAxis: {
           categories: categ,
           crosshair: true
       },
       yAxis: {
           min: 0,
           title: {
               text: 'Entrées - Sorties'
           }
       },
       tooltip: {
           headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
           pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
               '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
           footerFormat: '</table>',
           shared: true,
           useHTML: true
       },
       plotOptions: {
           column: {
               pointPadding: 0.2,
               borderWidth: 0
           }
       },
       series: [{
           name: 'Entrée',
           data: entrees

       }, {
           name: 'Sortie',
           data: sorties

       }]
   });
}
function evolutiondeseffectifsparsexe(homme,femme){
    Highcharts.chart('evolutiondeseffectifsparsexe', {

        title: {
            text: ''
        },

        subtitle: {
            text: ''
        },

        yAxis: {
            title: {
                text: 'Evolution par sexe'
            }
        },

        xAxis: {
            accessibility: {
                rangeDescription: 'Range: 2010 to 2017'
            }
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2017
            }
        },

        series: [{
            name: 'Homme',
            data: homme
        }, {
            name: 'Femme',
            data: femme
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
}
function repartitionpardepartement(dpt){
    Highcharts.chart('repartitionpardepartement', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: 'Repartition<br>par<br>département',
            align: 'center',
            verticalAlign: 'middle',
            y: 60
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%'],
                size: '110%'
            }
        },
        series: [{
            type: 'pie',
            name: 'département',
            innerSize: '50%',
            data:dpt
        }]
    });

}
function repartitionpartypedesortie(type_sort){
    Highcharts.chart('repartitionpartypedesortie', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'type de sortie',
            data: type_sort
        }]
    });
}



