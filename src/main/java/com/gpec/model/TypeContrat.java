package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class TypeContrat implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String libelle_type_contrat;
    @Column(unique=true)
    private String description_type_contrat;
    private int etat_type_contrat;

    @JsonIgnore
    @OneToMany(mappedBy = "type_contrat")
    private List<Contrat> contrats;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle_type_contrat() {
        return libelle_type_contrat;
    }

    public void setLibelle_type_contrat(String libelle_type_contrat) {
        this.libelle_type_contrat = libelle_type_contrat;
    }

    public String getDescription_type_contrat() {
        return description_type_contrat;
    }

    public void setDescription_type_contrat(String description_type_contrat) {
        this.description_type_contrat = description_type_contrat;
    }

    public int getEtat_type_contrat() {
        return etat_type_contrat;
    }

    public void setEtat_type_contrat(int etat_type_contrat) {
        this.etat_type_contrat = etat_type_contrat;
    }

    public List<Contrat> getContrats() {
        return contrats;
    }

    public void setContrats(List<Contrat> contrats) {
        this.contrats = contrats;
    }
}
