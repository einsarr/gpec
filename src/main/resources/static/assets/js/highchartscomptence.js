$(document).ready(function () {
    $.ajax({
            url:'radcontainercpt',
            dataType:"json",
            success:function (result) {
            console.log(result);
                var categories =[];
                var cptr=[];
                var cpta=[];
                for (let i = 0; i < result.length; i++) {
                  cptr=result[0];
                  cpta=result[1];
                  categories =result[2];
                }
                competences(categories,cptr,cpta)
            }
        });
});





function competences(categ,cptr,cpta) {
    Highcharts.chart('radcontainercpt', {

        chart: {
            polar: true,
            type: 'line'
        },

        accessibility: {
            description: ''
        },

        title: {
            text: 'Niveau requis & niveau acquis',
            x: -80
        },

        pane: {
            size: '80%'
        },

        xAxis: {
            categories: categ,
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical'
        },

        series: [{
            name: 'Niveau requis',
            data: cptr,
            pointPlacement: 'on'
        }, {
            name: 'Niveau acquis',
            data: cpta,
            pointPlacement: 'on'
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    pane: {
                        size: '70%'
                    }
                }
            }]
        }

    });

}