package com.gpec.controller;

import com.gpec.dao.DepartementRepository;
import com.gpec.dao.EmployeRepository;
import com.gpec.dao.RoleRepository;
import com.gpec.dao.UtilisateurRepository;
import com.gpec.model.Departement;
import com.gpec.model.Employe;
import com.gpec.model.Role;
import com.gpec.model.Utilisateur;
import com.gpec.service.SendMail;
import com.gpec.service.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
public class UserController implements Serializable {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private EmployeRepository employeRepository;

    @Autowired
    private DepartementRepository departementRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    BCryptPasswordEncoder encoder;
    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    public Utils utils;

    @GetMapping("/users")
    public String userPage(Model model) {
        Utilisateur utilisateur = new Utilisateur();
        model.addAttribute("utilisateur", utilisateur);
        List<Role> roles = roleRepository.findAll();
        List<Employe> employes = employeRepository.findAll();
        List<Departement> departements = departementRepository.findAll();
        List<Utilisateur> utilisateurs = utilisateurRepository.findAll();
        model.addAttribute("employes", employes);
        model.addAttribute("roles", roles);
        model.addAttribute("departements", departements);
        model.addAttribute("users", utilisateurs);
        return "securite/users";
    }
    @PostMapping("/addUser")
    public String addUser(@ModelAttribute("utilisateur") Utilisateur utilisateur) {
        if(Long.valueOf(utilisateur.getId())==null){
            String pwd = utils.getRandomNumberString();
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(utilisateur.getEmploye().getEmail_pers());
            message.setSubject("GPEC-DRH");
            message.setText("Mot de passe : "+pwd);
            this.emailSender.send(message);
            utilisateur.setPassword(encoder.encode(pwd));
            utilisateur.setEnabled(true);
        }
        try {
            utilisateurRepository.save(utilisateur);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/users";
    }

    @GetMapping("/user/{id}")
    public @ResponseBody
    Utilisateur getUser(@PathVariable(name = "id") int userId){
        return utilisateurRepository.findById(userId).get();
    }


    @PostMapping("/removeUser")
    public String removeUser(int userId) {
        Utilisateur u = utilisateurRepository.findById(userId).get();
        utilisateurRepository.delete(u);
        return "redirect:/users";
    }

    @PostMapping("/activerDesactiver")
    public String activerDesactiver(int userId,boolean etat) {
        Utilisateur u  = utilisateurRepository.findById(userId).get();
        u.setEnabled(etat);
        utilisateurRepository.save(u);
        return "redirect:/users";
    }

}
