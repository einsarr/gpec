package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class DiplomeAcademique implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private String code_dipl_acad;
    @Column(unique=true)
    private String libelle_dipl_acad;
    private String institut_ecole_prof;
    private int etat_dipl_acad;

    @JsonIgnore
    @OneToMany(mappedBy = "diplome_academique")
    private List<EmployeDiplomeAcademique> employe_diplomeAcademiques;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode_dipl_acad() {
        return code_dipl_acad;
    }

    public void setCode_dipl_acad(String code_dipl_acad) {
        this.code_dipl_acad = code_dipl_acad;
    }

    public String getLibelle_dipl_acad() {
        return libelle_dipl_acad;
    }

    public void setLibelle_dipl_acad(String libelle_dipl_acad) {
        this.libelle_dipl_acad = libelle_dipl_acad;
    }

    public String getInstitut_ecole_prof() {
        return institut_ecole_prof;
    }

    public void setInstitut_ecole_prof(String institut_ecole_prof) {
        this.institut_ecole_prof = institut_ecole_prof;
    }

    public int getEtat_dipl_acad() {
        return etat_dipl_acad;
    }

    public void setEtat_dipl_acad(int etat_dipl_acad) {
        this.etat_dipl_acad = etat_dipl_acad;
    }

    public List<EmployeDiplomeAcademique> getEmploye_diplomeAcademiques() {
        return employe_diplomeAcademiques;
    }

    public void setEmploye_diplomeAcademiques(List<EmployeDiplomeAcademique> employe_diplomeAcademiques) {
        this.employe_diplomeAcademiques = employe_diplomeAcademiques;
    }
}
