package com.gpec.dao;

import com.gpec.model.PosteTache;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PosteTacheRepository extends JpaRepository<PosteTache, Integer> {
}
