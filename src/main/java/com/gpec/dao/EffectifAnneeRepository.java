package com.gpec.dao;

import com.gpec.model.EffectifAnnee;
import com.gpec.model.Employe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface  EffectifAnneeRepository extends JpaRepository<EffectifAnnee, Integer> {
    /**
     * Calcul du taux de turn over
     * @return
     */
    @Query(value = "SELECT ((effectif_sortant+effectif_entrant/2)/effectif_janvier)*100 as taux_turn_over FROM EffectifAnnee where etat = 1")
    public int tauxTurnOver();

    @Query("SELECT e FROM EffectifAnnee e WHERE e.etat=1")
    public EffectifAnnee findEffectifAnneeByEtat();

    @Modifying
    @Query("UPDATE EffectifAnnee e SET e.etat=0")
    public void nouvelAn();


}
