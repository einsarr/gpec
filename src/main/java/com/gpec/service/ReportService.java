package com.gpec.service;

import com.gpec.dao.EmployeRepository;
import com.gpec.model.Employe;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportService {

    @Autowired
    private EmployeRepository employeRepository;

    public String exportReport(String reportFormat) throws FileNotFoundException, JRException {
        String path="F:\\MEMOIRE\\gpec";
        List<Employe> employes = employeRepository.findAll();
        File file = ResourceUtils.getFile("classpath:employes.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(employes);
        Map<String, Object> parameters =new HashMap<>();
        parameters.put("createdBy","Java Techie");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if(reportFormat.equalsIgnoreCase("html")){
            JasperExportManager.exportReportToHtmlFile(jasperPrint,path+"\\employes.html");
        }
        if(reportFormat.equalsIgnoreCase("pdf")){
            JasperExportManager.exportReportToPdfFile(jasperPrint,path+"\\employes.pdf");
        }
        return "report genered in path : "+path;

    }
}
