package com.gpec.controller;

import com.gpec.dao.*;
import com.gpec.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class HomeController {

    @Autowired
    private EmployeRepository employeRepository;
    @Autowired
    private PosteRepository posteRepository;
    @Autowired
    private CompetenceRepository competenceRepository;
    @Autowired
    private TacheRepository tacheRepository;
    @Autowired
    private ExigencePosteRepository exigencePosteRepository;
    @Autowired
    private AnneeRepository anneeRepository;

    @RequestMapping("/profile")
    public String getProfile(Model model) {
        try {
            Utilisateur user =(Utilisateur) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            Employe employe = employeRepository.findEmployeByUtilisateur(user);
            Poste poste = posteRepository.findPosteByEmploye(user.getEmploye().getId());
            List<ExigencePoste> exigencePostes = exigencePosteRepository.getExigencesByPoste(poste.getId());
            List<Tache> taches = tacheRepository.getTacheByPoste(poste.getId());
            List<Competence> competences = competenceRepository.getCompetenceByPoste(poste.getId());

            model.addAttribute("exigencePostes", exigencePostes);
            model.addAttribute("taches", taches);
            model.addAttribute("competences", competences);
            model.addAttribute("poste", poste);
            model.addAttribute("employe", employe);
        }catch (Exception e){
            System.out.println(e.fillInStackTrace());
        }

        return "employe/profile";
    }

    @RequestMapping("/profilea")
    public String profilea(Model model,@RequestParam("id") long employe_id) {
        try {
            Employe employe = employeRepository.findById(employe_id).get();
            Poste poste = posteRepository.findPosteByEmploye(employe_id);
            List<ExigencePoste> exigencePostes = exigencePosteRepository.getExigencesByPoste(poste.getId());
            List<Tache> taches = tacheRepository.getTacheByPoste(poste.getId());
            List<Competence> competences = competenceRepository.getCompetenceByPoste(poste.getId());
            model.addAttribute("exigencePostes", exigencePostes);
            model.addAttribute("taches", taches);
            model.addAttribute("competences", competences);
            model.addAttribute("poste", poste);
            model.addAttribute("employe", employe);
        }catch (Exception e){
            System.out.println(e.fillInStackTrace());
        }
        return "employe/profile";
    }

    @RequestMapping("/radcontainercpt")
    public ResponseEntity<?> competencesEvaluation(@RequestParam(value = "id",defaultValue = "0") long employe_id){
        List<Employe> employes = employeRepository.findAll();
        List<String> competences = new ArrayList<>();
        List<Integer> competencesreq = new ArrayList<>();
        List<Integer> competencesacq = new ArrayList<>();
        List<List> cptrcpta = new ArrayList<>();
        Employe employe = null;
        if(employe_id!=0){
            employe = employeRepository.findById(employe_id).get();
        }else{
            Utilisateur user =(Utilisateur) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            employe = employeRepository.findEmployeByUtilisateur(user);
        }
        for (Employe emp:employes){
            if(emp.getId()==employe.getId()){
                for(EmployePoste empPos: emp.getPoste_employes()){
                    for(PosteCompetence pcpt: empPos.getPoste().getPoste_competences()){
                        competences.add(pcpt.getCompetence().getCode_competence());
                        competencesreq.add(pcpt.getCompetence().getNiveauMaitriseCptRequis().getValeur_niv());
                        for(Evaluation ev: pcpt.getCompetence().getEvaluations()){
                            if(ev.getAnnee()==anneeRepository.anneeEnCours() && ev.getEmploye()==employe)
                            competencesacq.add(ev.getNiveauMaitriseCptAquis().getValeur_niv());
                        }
                    }
                }
            }
        }
        cptrcpta.add(competencesreq);cptrcpta.add(competencesacq);
        cptrcpta.add(competences);
        return new ResponseEntity<>(cptrcpta, HttpStatus.OK);
    }

    @RequestMapping("/dashboard")
    public String home()
    {
        return "layouts/dashboard";
    }

}
