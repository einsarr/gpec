package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "UniqueNumberAndStatus", columnNames = { "employe_id", "annee_id" }) })
public class EntretienAnnuel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "employe_id")
    private Employe employe;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "annee_id")
    private Annee annee;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "qualification_id")
    private Qualification qualification;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "efficacite_id")
    private Efficacite efficacite;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "potentiel_id")
    private Potentiel potentiel;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public Annee getAnnee() {
        return annee;
    }

    public void setAnnee(Annee annee) {
        this.annee = annee;
    }

    public Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    public Efficacite getEfficacite() {
        return efficacite;
    }

    public void setEfficacite(Efficacite efficacite) {
        this.efficacite = efficacite;
    }

    public Potentiel getPotentiel() {
        return potentiel;
    }

    public void setPotentiel(Potentiel potentiel) {
        this.potentiel = potentiel;
    }
}
