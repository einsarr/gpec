package com.gpec.service;
import com.gpec.dao.AnneeRepository;
import com.gpec.dao.EffectifAnneeRepository;
import com.gpec.dao.EmployeRepository;
import com.gpec.model.Annee;
import com.gpec.model.EffectifAnnee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;

@Component
public class Scheduled {

    @Autowired
    private EffectifAnneeRepository effectifAnneeRepository;

    @Autowired
    private EmployeRepository employeRepository;

    @Autowired
    private AnneeRepository anneeRepository;

    /**
     * Effectif au 31 décembre
     */
    @Transactional
    @org.springframework.scheduling.annotation.Scheduled(cron = "0 0 0 1 1 *")
    public void effectif1Javnier(){
        //Annee anneeEncours =anneeRepository.getById(anneeRepository.anneeEnCours());
        int eff = employeRepository.countEmployeById();
        int efffemme = employeRepository.countEmployeFemme();
        int effhomme = employeRepository.countEmployeHomme();
        EffectifAnnee effectifAnnee = new EffectifAnnee();
        effectifAnnee.setEffectif_janvier(eff);
        effectifAnnee.setEffectif_entrant(eff);
        effectifAnnee.setEffectif_sortant(eff);
        effectifAnnee.setEffectif_f_janvier(efffemme);
        effectifAnnee.setEffectif_h_janvier(effhomme);
        effectifAnnee.setEtat(1);
        //effectifAnnee.setAnnee(anneeRepository.anneeEnCours());
        effectifAnneeRepository.nouvelAn();
        effectifAnneeRepository.save(effectifAnnee);
    }

    @Transactional
    @org.springframework.scheduling.annotation.Scheduled(cron = "0 0 0 1 1 *")
    public void nouvelleAnnee(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        Annee annee = new Annee();
        annee.setCode_annee(calendar.get(Calendar.YEAR));
        annee.setLibelle_annee("Année "+calendar.get(Calendar.YEAR));
        annee.setEtat_annee(1);
        anneeRepository.updateAnn();
        anneeRepository.save(annee);
    }
}
