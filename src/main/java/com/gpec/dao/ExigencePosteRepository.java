package com.gpec.dao;

import com.gpec.model.ExigencePoste;
import com.gpec.model.Tache;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExigencePosteRepository extends JpaRepository<ExigencePoste, Integer> {

    @Query(value = "SELECT * FROM exigence_poste ep join poste p " +
            "on ep.id=p.exigence_id WHERE p.id=:posteId",nativeQuery = true)
    public List<ExigencePoste> getExigencesByPoste(@Param("posteId")Long posteId);



}
