package com.gpec.controller;

import com.gpec.dao.EmployePosteRepository;
import com.gpec.dao.EmployeRepository;
import com.gpec.dao.PosteRepository;
import com.gpec.model.Employe;
import com.gpec.model.EmployePoste;
import com.gpec.model.Poste;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Controller
public class EmployePosteController implements Serializable {
    @Autowired
    private EmployePosteRepository employePosteRepository;
    @Autowired
    private EmployeRepository employeRepository;
    @Autowired
    private PosteRepository posteRepository;

    @GetMapping("/employe_postes")
    public String EmployePostesPage(Model model) {
        EmployePoste employe_poste          = new EmployePoste();
        List<EmployePoste> employe_postes   = employePosteRepository.findAll();
        List<Employe> employes              = employeRepository.findAll();
        List<Poste> postes                  = posteRepository.findAll();
        model.addAttribute("employe_poste", employe_poste);
        model.addAttribute("employe_postes", employe_postes);
        model.addAttribute("employes", employes);
        model.addAttribute("postes", postes);
        return "employes/employe_postes";
    }
    @Transactional
    @PostMapping("/addEmployePoste")
    public String addEmployePoste(@ModelAttribute("employe_poste") EmployePoste employe_poste) {
        if(employe_poste.getEmploye()!=null){
            for(EmployePoste emp: employe_poste.getEmploye().getPoste_employes()){
                if(emp.getEtat()==1)
                {
                    emp.setEtat(0);
                    emp.setDate_fin(new Date());
                    try {
                        employePosteRepository.save(emp);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
        try {
            employePosteRepository.save(employe_poste);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/employe_postes";
    }

    @GetMapping("/employe_poste/{id}")
    public @ResponseBody
    EmployePoste getEmployePoste(@PathVariable(name = "id") int employe_posteId){
        return employePosteRepository.findById(employe_posteId).get();
    }

    @PostMapping("/removeEmployePoste")
    public String removeEmployePoste(int employe_posteId) {
        EmployePoste g = employePosteRepository.findById(employe_posteId).get();
        employePosteRepository.delete(g);
        return "redirect:/employe_postes";
    }
}
