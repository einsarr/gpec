package com.gpec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Qualification implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique=true)
    private int valeur_qualification;
    @Column(unique=true)
    private String libelle_qualification;
    private int etat_qualification;

    @JsonIgnore
    @OneToMany(mappedBy = "qualification")
    private List<EntretienAnnuel> entretienAnnuels;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValeur_qualification() {
        return valeur_qualification;
    }

    public void setValeur_qualification(int valeur_qualification) {
        this.valeur_qualification = valeur_qualification;
    }

    public String getLibelle_qualification() {
        return libelle_qualification;
    }

    public void setLibelle_qualification(String libelle_qualification) {
        this.libelle_qualification = libelle_qualification;
    }

    public int getEtat_qualification() {
        return etat_qualification;
    }

    public void setEtat_qualification(int etat_qualification) {
        this.etat_qualification = etat_qualification;
    }
}
