package com.gpec.dao;

import com.gpec.model.Competence;
import com.gpec.model.Employe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompetenceRepository extends JpaRepository<Competence, Long> {


    @Query(value = "SELECT * from competence c join poste_competence pc " +
            "on pc.competence_id=c.id join poste p on pc.poste_id=p.id WHERE p.id=:posteId",nativeQuery = true)
    List<Competence> getCompetenceByPoste(@Param("posteId")Long posteId);

    @Query(value = "SELECT * from competence WHERE est_matrice=1",nativeQuery = true)
    List<Competence> getCompetenceMatrice();

    @Query(value = "SELECT max(id) from competence",nativeQuery = true)
    long getLastCompetence();


}
