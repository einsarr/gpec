package com.gpec.dao;

import com.gpec.model.PosteCompetence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PosteCompetenceRepository extends JpaRepository<PosteCompetence, Integer> {
}
