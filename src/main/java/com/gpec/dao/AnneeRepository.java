package com.gpec.dao;

import com.gpec.model.Annee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AnneeRepository extends JpaRepository<Annee, Integer> {
    /**
     * Année en cours
     * @return
     */
    @Query(value = "SELECT a.id FROM annee a WHERE a.etat_annee=1",nativeQuery = true)
    int  anneeEnCours1();
    @Query(value = "SELECT a FROM Annee a WHERE a.etat_annee=1")
    Annee anneeEnCours();

    @Modifying
    @Query(value = "UPDATE annee a SET a.etat_annee=0",nativeQuery = true)
    void updateAnn();

}
