package com.gpec.controller;

import com.gpec.dao.ActiviteRepository;
import com.gpec.dao.TacheRepository;
import com.gpec.model.Activite;
import com.gpec.model.Tache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
@PreAuthorize("hasAuthority('ROLE_DIRECTEUR')")
public class TacheController implements Serializable {
    @Autowired
    private TacheRepository tacheRepository;

    @Autowired
    private ActiviteRepository activiteRepository;

    @GetMapping("/taches")
    public String tachePage(Model model) {
        Tache tache                 = new Tache();
        List<Activite> activites    = activiteRepository.findAll();
        List<Tache> taches          = tacheRepository.findAll();
        model.addAttribute("activites", activites);
        model.addAttribute("tache", tache);
        model.addAttribute("taches", taches);
        return "parametrage/taches";
    }
    @PostMapping("/addTache")
    public String addTache(@ModelAttribute("tache") Tache tache) {
        try {
            tacheRepository.save(tache);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/taches";
    }

    @GetMapping("/tache/{id}")
    public @ResponseBody
    Tache getTache(@PathVariable(name = "id") int tacheId){
        return tacheRepository.findById(tacheId).get();
    }


    @PostMapping("/removeTache")
    public String removeTache(int tacheId) {
        Tache g = tacheRepository.findById(tacheId).get();
        tacheRepository.delete(g);
        return "redirect:/taches";
    }
}
