package com.gpec.controller;

import com.gpec.dao.MissionRepository;
import com.gpec.dao.PosteMissionRepository;
import com.gpec.dao.PosteRepository;
import com.gpec.model.Mission;
import com.gpec.model.Poste;
import com.gpec.model.PosteMission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Controller
@PreAuthorize("hasAuthority('ROLE_DIRECTEUR')")
public class PosteMissionController implements Serializable {
    @Autowired
    private PosteMissionRepository posteMissionRepository;

    @Autowired
    private PosteRepository posteRepository;

    @Autowired
    private MissionRepository missionRepository;

    @GetMapping("/poste_missions")
    public String PosteMissionsPage(Model model) {
        PosteMission poste_mission          = new PosteMission();
        List<Poste> postes                  = posteRepository.findAll();
        List<Mission> missions              = missionRepository.findAll();
        List<PosteMission> poste_missions   = posteMissionRepository.findAll();
        model.addAttribute("poste_mission", poste_mission);
        model.addAttribute("poste_missions", poste_missions);
        model.addAttribute("postes", postes);
        model.addAttribute("missions", missions);
        return "parametrage/poste_missions";
    }
    @PostMapping("/addPosteMission")
    public String addPosteMission(@ModelAttribute("poste_mission") PosteMission poste_mission) {
        try {
            posteMissionRepository.save(poste_mission);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/poste_missions";
    }

    @GetMapping("/poste_mission/{id}")
    public @ResponseBody
    PosteMission getPosteMission(@PathVariable(name = "id") int poste_missionId){
        return posteMissionRepository.findById(poste_missionId).get();
    }


    @PostMapping("/removePosteMission")
    public String removePosteMission(int poste_missionId) {
        PosteMission g = posteMissionRepository.findById(poste_missionId).get();
        posteMissionRepository.delete(g);
        return "redirect:/poste_missions";
    }
}
