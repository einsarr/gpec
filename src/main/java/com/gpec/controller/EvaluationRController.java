package com.gpec.controller;

import com.gpec.dao.*;
import com.gpec.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
//@CrossOrigin(origins = "*")
public class EvaluationRController implements Serializable {

    @Autowired
    private EvaluationRepository evaluationRepository;
    @Autowired
    private CompetenceRepository competenceRepository;
    @Autowired
    private EmployeRepository employeRepository;
    @Autowired
    private AnneeRepository anneeRepository;
    @Autowired
    private QualificationRepository qualificationRepository;
    @Autowired
    private EfficaciteRepository efficaciteRepository;
    @Autowired
    private PotentielRepository potentielRepository;
    @Autowired
    private EntretienAnnuelRepository entretienAnnuelRepository;
    @Autowired
    private NiveauMaitriseCptRequisRepository niveauRepo;
    @RequestMapping(value = "/addEvaluation",method = RequestMethod.POST)
    public ResponseEntity<Evaluation> addEvaluation(@RequestBody String notes) {
        String[] val= notes.split("-");
        Evaluation evaluation = new Evaluation();
        long un = Long.parseLong(val[0]);
        long deux = Long.parseLong(val[1]);
        int trois = Integer.parseInt(val[2]);
        evaluation.setCompetence(competenceRepository.findById(un).get());
        evaluation.setEmploye(employeRepository.findById(deux).get());
        evaluation.setNiveauMaitriseCptAquis(niveauRepo.findById(trois).get());
        evaluation.setAnnee(anneeRepository.anneeEnCours());
        try{
            evaluationRepository.save(evaluation);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(evaluation, HttpStatus.OK);
    }

    @RequestMapping(value = "/addEntretien",method = RequestMethod.POST)
    public ResponseEntity<EntretienAnnuel>  addEntretien(@RequestBody String notes){
        Annee annee = anneeRepository.anneeEnCours();
        String[] val= notes.split("&");
        String[] l_emp= val[0].split("=");
        String[] l1= val[1].split("=");
        String[] l2= val[2].split("=");
        String[] l3= val[3].split("=");
        long emp = Long.parseLong(l_emp[1]);
        int q = Integer.parseInt(l1[1]),e = Integer.parseInt(l2[1]),p=Integer.parseInt(l3[1]);
        EntretienAnnuel entretienAnnuel = new EntretienAnnuel();
        entretienAnnuel.setEmploye(employeRepository.findById(emp).get());
        entretienAnnuel.setQualification(qualificationRepository.findById(q).get());
        entretienAnnuel.setEfficacite(efficaciteRepository.findById(e).get());
        entretienAnnuel.setPotentiel(potentielRepository.findById(p).get());
        entretienAnnuel.setAnnee(annee);
        try {
            entretienAnnuelRepository.save(entretienAnnuel);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return new ResponseEntity<>(entretienAnnuel, HttpStatus.OK);
    }


}
