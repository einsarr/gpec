package com.gpec.dao;

import com.gpec.model.Employe;
import com.gpec.model.EmployeDepartement;
import com.gpec.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeRepository extends JpaRepository<Employe, Long> {
    /**
     * Retourner les employés en activité
     * @return
     */
    @Query("SELECT e FROM Employe e WHERE e.etat=1")
    List<Employe> findEmployeEtatActif();
    /***
     * Liste des employés évalués
     * @return
     */
    @Query(value = "SELECT * from employe e WHERE e.id IN(select ev.employe_id FROM evaluation ev) AND e.etat=1",nativeQuery = true)
    List<Employe>employesEvalues();


    /**
     * retourner un employé sachant son id
     * @param id
     * @return
     */
    @Query("SELECT e FROM Employe e WHERE e.id=:id")
    Employe getEmployeById(@Param("id")Long id);

    /**
     * Retourne un employé sachant le user associé
     * @param user
     * @return
     */
    @Query("SELECT e FROM Employe e WHERE e.utilisateur=:user")
    Employe findEmployeByUtilisateur(@Param("user") Utilisateur user);

    /**
     * Nombre d'employé
     * @return
     */
    @Query(value = "SELECT count(id) FROM Employe where etat = 1")
    int countEmployeById();

    /**
     * Effectif de femme
     * @return
     */

    @Query(value = "SELECT count(id) FROM Employe where sexe='F' AND etat = 1")
    int countEmployeFemme();
    /**
     * Effectif hommes
     */

    @Query(value = "SELECT count(id) FROM Employe WHERE sexe='M' AND etat = 1")
    int countEmployeHomme();

    /**
     * nombre d'employé qui doivent aller à la retraite dans moins de 2 ans
     * @return
     */
    @Query(value = "SELECT count(e.id) FROM Employe e WHERE (YEAR(CURRENT_DATE)-YEAR(e.date_naissance))>57 ",nativeQuery = true)
    int countEmployeByDate_naissance();

    @Query(value = "SELECT * FROM Employe e WHERE (YEAR(CURRENT_DATE)-YEAR(e.date_naissance))>57 ",nativeQuery = true)
    List<Employe> listeEmployeRetraites();

    /**
     * Employé par département
     * @param departementId
     * @return
     */
    @Query(value = "SELECT * from employe e join employe_departement ed " +
            "on ed.employe_id=e.id join departement d on ed.departement_id=d.id WHERE e.etat=1 AND d.id=:departementId",nativeQuery = true)
    List<Employe>employeByDepartement(@Param("departementId")int departementId);

    /**
     * Repartition par sexe
     * @return
     */
//    @Query(value = "SELECT e.id,e.sexe, count(e.sexe) FROM Employe e GROUP BY e.id,e.sexe",nativeQuery = true)
    @Query(value=" SELECT *,count(*) AS count_gender FROM employe GROUP BY sexe", nativeQuery = true)
    List<Employe> repartitionParSexe();


    //@Query(value = "SELECT *,(e.id) FROM Employe e WHERE YEAR(e.DATE_RECRUTEMENT) ",nativeQuery = true)
    @Query(value = "SELECT *, YEAR(e.DATE_RECRUTEMENT) annee, COUNT(YEAR(e.DATE_RECRUTEMENT)) FROM Employe AS e GROUP BY annee",nativeQuery = true)
    List<Employe> repartitionparAnciennete();
}
