package com.gpec.dao;

import com.gpec.model.Activite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ActiviteRepository extends JpaRepository<Activite, Integer> {

    @Query(value = "SELECT max(id) from activite",nativeQuery = true)
    long getLastActivite();
}
