package com.gpec.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class DefaultController {

    @RequestMapping("/logout")
    public String destroySession(HttpServletRequest request) {
        request.getSession().invalidate();
        return "redirect:/";
    }

    @GetMapping("/403")
    public String accessdenied(Model model){
        return "403";
    }

    @GetMapping("/404")
    public String notfound(Model model){
        return "404";
    }

    @GetMapping("/500")
    public String servererreur(Model model){
        return "500";
    }

}
