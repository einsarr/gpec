package com.gpec.dao;

import com.gpec.model.Employe;
import com.gpec.model.MotifAbsence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MotifAbsenceRepository extends JpaRepository<MotifAbsence, Integer> {
    /***
     * Liste des motifs dans absence
     * @return
     */
    @Query(value = "SELECT * from motif_absence m WHERE m.id IN(select a.motif_absenc_id FROM absence a)",nativeQuery = true)
    List<MotifAbsence> motifAbsences();
}
